# Beta Testing Protocol Pioneer

## Introduction

This lab involved beta testing the "Protocol Pioneer" game created by LCDR Downs by playing through the first two chapters to learn about how sending messages over a network works and noting any bugs along the way.

## Collaboration

I did not collaborate with anyone else on this lab.

## Process

I followed all instructions on the course page as well as the gitlab repo.

## Lab Questions

1. I solved Chapter 1 by including an "if statement" in the section of the provided code that analyzed incoming messages. My code sends a response message to the distress call in the direction that was provided in the interface field of the message. The code I included was:

    ````python

        if m.interface == "W":
            self.send_message("Message Received", "W")
        elif m.interface == "E":
            self.send_message("Message Received", "E")
        elif m.interface == "N":
            self.send_message("Message Received", "N")
        elif m.interface == "S":
            self.send_message("Message Received", "S")

    ````

2. I solved Chapter 2 with the following code:

    ```python
        # process message
        self.state["received"][m.interface][0] += 1
        self.state["received"][m.interface][1] += int(m.text)

        if self.state["received"][m.interface][0] == 3:
            self.send_message(self.state["received"][m.interface][1], m.interface)
    ```

    This code analyzes each message in the queue and increments both the message count and the sum based on the interface it received the message from. After a message has been received from an interface three times, I send back the sum to that interface.

3. Beta Testing Notes

    A bug I found was when the repair bots are analyzing the message that the user sends back to the bot. For example, I frequently got the error `Ratchet 03 - calibration failed! Received message 9 but expected 9`. My inclination was that this is some how a type mismatch error, so I looked at the source code for how the repair bot was comparing my answer to the expected answer. I found that it was expecting the user to send it a string, so I had to convert my message from an int to a string before I sent it, and that fixed the problem.

    I was consistently confused about where my code was supposed to go. I did not know if I had to leave all of the prewritten code or if I was supposed to replace all of it.

    I think the amount of narrative was good. My suggestion would be to include more dialogue about how the chapter relates to concepts from class. It was obvious that the focus of the two chapters was to address how sending messages over a network happens, but I think it would be more useful is the chapters focused on more specific topics like HOL blocking or subnets or prefix matching or something like that.

    I thought the game was fun, but it was not always clear how the game chapters related to class content.
