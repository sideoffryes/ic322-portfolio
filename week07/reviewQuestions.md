# Review Question Answers

### R11 Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers)

In general, packet loss occurs when data is arriving faster than it can be processed. In this situation, input buffers attempt to store all incoming packets that cannot be immediately processed. If packets are arriving too fast, the buffer will overflow, meaning that it will run out of space to store more data. The link will have to either drop new incoming packets or drop existing packets already in the buffer.

Packet loss at input ports can be eliminated by increasing the speed of the switching fabric such that it can handle packets at least as fast as they are coming in.

### R12 Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?

Packet loss can occur at output ports when packets are be processed faster than they can be transmitted. After a packet has been processed, it will enter the output buffer and wait to be transmitted (actually put onto the transmission medium). If packets are being processed faster than they can be transmitted, the output buffer will begin to fill with packets. Once the buffer is full, either new packets or existing packets (already in the buffer) will have to be dropped.

Increasing the switch fabric speed will *not* prevent this issue. Actually, it will exacerbate the issue by causing a greater build up of packets in the buffer. The solution to this problem is to upgrade the link to support a higher transmission speed.

### R13 What is HOL blocking? Does it occur in input ports or output ports?

**HOL blocking** describes the scenario where a packet in a queue cannot be transmitted to its free output port because the packet in front of it in the queue is waiting to be transmitted to a port which is busy. This issue occurs at *input ports*.

### R16 What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?

The difference is that RR scheduling will transmit packets from each class queue at an equal frequency. WFQ scheduling will transmit packets from queues which have greater weights more frequently. For example, in RR scheduling, if there are 5 queues, each queue with get ${1 \over 5}$ of the bandwidth. In WFQ scheduling, however, if there is one queue with a weight of .5 and two queues with weights of .25, the .5 queue with get ${1 \over 2}$ of the bandwidth while the other two queues each get ${1 \over 4}$ of the bandwidth.

The only case where RR and WFQ scheduling will behave the same is if all of the weights in the WFQ scheduling are equal.

### R18 What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?

The **time-to-live** field will limit how many routers the packet goes through, so you would set the value of the field to N.

### R21 Do routers have IP addresses? If so, how many?

The router has **1 IP address for the WAN interface** and **1 IP address for the LAN interface**.

### P4 Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?

![P4 Diagram](Week07P4.png "Diagram from textbook for P4")

1. 3 time slots. Packet X from port 1 and packet Y from port 2 will be transmitted at the same time, but packet Y from port 3 must wait. During the second time slot, packet X from port 2 will transmit at the same time as packet Y from port 3. During the third time slot, packet Z will transmit.

2. In the worst case situation, only one packet total can be transmitted at a time, and since there are 5 packets, then we would need 5 time slots in the worst case scenario.

### P5 Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes

#### P5a Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .)

1123112311231123

#### P5b Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?

112112112112

### P8 Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows

![P8 Diagram](Week07P8.png "Diagram from textbook for P8")

#### P8a Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces

| Prefix            | Link Interface    |
| -                 | -                 |
| 11100000 00       | 0                 |
| 11100000 010000   | 1                 |
| 1110000           | 2                 |
| Otherwise         | 3                 |

#### P8b Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses: 11001000 10010001 01010001 01010101, 11100001 01000000 11000011 00111100, 11100001 10000000 00010001 01110111

- 11001000 10010001 01010001 01010101: the prefix *11001* does not match any of the prefixes in the table, so it gets sent to link interface 3.

- 11100001 01000000 11000011 00111100: the prefix *1110000* matches a prefix in the table, so the packet is sent to link interface 2.

- 11100001 10000000 00010001 01110111: the prefix *1110000* matches a prefix in the table, so the packet is sent to link interface 2.

### P9 Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table. For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range

![P9 Diagram](Week07P9.png "Diagram from textbook for P9")

- 0: 00000000 00000000 00000000 00000000 through 00111111 11111111 11111111 11111111

- 1: 01000000 00000000 00000000 00000000 through 01011111 11111111 11111111 11111111

- 2: 01100000 00000000 00000000 00000000 through 10111111 11111111 11111111 11111111

- 3: 11000000 00000000 00000000 00000000 through 11111111 11111111 11111111 11111111

### P11 Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints

Subnet 1: 223.1.17.0/26

Subnet 2: 223.1.17.60/24

Subnet 2: 223.1.17.150/26
