# Week 07: The Network Layer: Data Plane
*MIDN 2/C Henry Frye, IC322 Fall AY24*

This week, we moved on from our discussion of the Transport Layer and began a new discussion about the Network Layer, starting with the Data Plane.

## Link to this week's work
[Learning Goals](learningGoals.md)

[Review Questions](reviewQuestions.md)

[Beta Testing Protocol Pioneer](lab.md)
