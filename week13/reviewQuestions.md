# Review Question Answers

## Chapter 6

### R10 Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?

Yes, C's adapter will process all of the frames.

C's adapter will not pass the frames to the network layer, because it will examine the MAC address, realize that it is not the intended recipient, and drop all of the frames.

If A send the frames with the MAC broadcast address, they will be processed and passed to the network layer by C.

### R11 Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?

An ARP query is sent within a broadcast frame because the sender does not know the destination MAC address, so it must send it to all available hosts in hopes of reaching the intended destination.

An ARP response is sent with a specific destination, because the sender can analyze the ARP query that was received and use its source address to set the destination address for the response message.

### P14 Consider three LANs interconnected by two routers, as shown in Figure 6.33

![Figure 6.33](Week13Ch6P15.png)

#### P14a Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx

**Subnet 1**:

Router Left: 192.168.1.1

A: 192.168.1.2

B: 192.168.1.3

**Subnet 2**:

Router Right: 192.168.2.1

C: 192.168.2.2

D: 192.168.2.3

Router Left: 192.168.2.4

**Subnet 3**:

Router Right: 192.168.3.1

E: 192.168.3.2

F: 192.168.3.3

#### P14b Assign MAC addresses to all of the adapters

**Subnet 1**:

Router Left: AA:AA:AA:AA:AA:AA

A: AA:AA:AA:AA:AA:AB

B: AA:AA:AA:AA:AA:BA

**Subnet 2**:

Router Right: AA:AA:AA:AA:AA:BB

C: AA:AA:AA:AA:AB:AA

D: AA:AA:AA:AA:AB:AB

Router Left: AA:AA:AA:AA:AB:BB

**Subnet 3**:

Router Right: AA:AA:AA:AA:BA:AA

E: AA:AA:AA:AA:BA:AB

F: AA:AA:AA:AA:BA:BB

#### P14c Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1

1) E creates IP datagram with destination set to the IP address of B
2) E encapsulates the IP datagram in a link-layer frame with the destination set to the MAC address of B's adapter
3) The frame departs E, enters the switch, the switch forwards it to the router, the router, the router sends it to the switch in the middle, that switch forwards it to the router on the left, the router sends it to the left switch, and the switch forwards it to B

#### P14d Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date)

1) E creates IP datagram with destination set to IP of B
2) E broadcast to get the MAC address corresponding to B's IP address
3) The query is sent to all of the hosts in the network. All hosts except B drop the frame.
4) B received the frame and sends an ARP response to E with its IP address.
5) E receives B's MAC address and continues with the steps from (c).

### P15 Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1

![Figure 6.33](Week13Ch6P15.png)

#### P15a Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?

No. The message does not need to go through the router since both hosts are on the same subnet.

The source IP and MAC addresses are the IP and MAC address of E. The destination IP and MAC addresses are the IP and MAC address of F.

#### P15b Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP and MAC addresses?

Yes, E will perform an ARP query that will be answered by either R1 or F since they both have B in their ARP cache.

Source: IP and MAC address of E.

Destination: IP and MAC of B.

#### P15c Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?

When S1 receives the message, it will check its router table and forwards the message to the interface that B is connected to.

It will forward the message to R1, but the message will immediately get dropped since the IPs don't match.

B does not need to send an ARP query because it can get A's MAC address from the ARP query that A sent to B.

After S1 gets an ARP response message, it will add host B to its table and pass the response to A.

## Chapter 7

### R3 What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?

- **path loss**: a signal dispersing while traveling through open space, resulting in decreased signal strength.

- **multipath propagation**: portions of the electromagnetic wave are reflected off surfaces and take different paths of different lengths from the sender to the receiver. The signal is "blurred" at the receiver, and multipath propagation can change over time due to objects moving in between the sender and receiver.

- **interference from other sources**: some signal sources transmit at the same frequency, and the signals interfere with each other.

### R4 As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?

The station can either increase the power of its transmissions or decrease the rate at which transmissions are sent.

### P6 In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?

Even though the channel was detected to be idle, that might no longer be true. Another station might have already detected that the channel was idle and began transmitting its own message, so the channel must be "sensed" again to determine if it is still idle.

### P7 Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment

The total amount of time = DIFS + SIFS + SIFS + SIFS = **DIFS + 3(SIFS)**.
