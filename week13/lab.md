# Week 13 Lab

## Wireshark ARP Lab

## Introduction

This lab was about using wireshark to inspect packets exchanged using the Ethernet and ARP protocols.

## Collaboration

I did not collaborate with anyone during this lab.

## Process

I followed all of the lab instructions found on the textbook's website, and I used the pre-captured files provided by the textbook for the analysis.

For questions 19 and 20, the ARP request that I initially inspected did not receive a reply, so I picked the only reply message available in the capture.

## Lab Questions

1. Ethernet address of *my computer*: **c4:41:1e:75:b1:52**

2. *Destination* address of the ethernet frame: **00:1e:c1:7e:d9:01**. This is **not** the ethernet address of gaia.cs.usmass.edu. This is the address of the first router the frame will arrive at.

3. The hex value of the frame type field is **0x0800**, which indicates **IPv4**.

4. The ASCII "G" shows up at the **43rd** byte in the frame.

5. The value of the *source* address is: **00:1e:c1:7e:d9:01**. This is the address of the ethernet interface of the **router that our computer is connected to**.

6. The value of the *destination* address is: **c4:41:1e:75:b1:52**. This is the address of my computer.

7. The hex value of the frame type is: **0x0800**, which indicates **IPv4**.

8. The ASCII character "O" did **not show up in the Ethernet frame**, it was only in the reconstructed TCP message.

9. There are **4 frames** carrying the data.

10. My school laptop's permissions would not allow me to execute the *arp* command, so I ran it on a wsl instance. I had **1 ARP entry**.

11. Each ARP entry contains the **destination IP and destination MAC addresses** of a domain.

12. The hex value of the *source address* is **00:1e:c1:7e:d9:01**.

13. The hex value of the *destination address* is **ff:ff:ff:ff:ff:ff**. This address does not correspond to any particular device, because this is the **broadcast address**. This indicates that the frame should be send to all hosts on the network.

14. The hex value for the *frame type field* is **0x0806**, which indicates **ARP**.

15. **20 bytes**.

16. The value of the opcode is **1**, which indicates that it is an **ARP request**.

17. **Yes**, the ARP request contains the IP address of the sender. The value is **128.119.247.1**.

18. The IP address of the device whose corresponding Ethernet address is being requested is **128.119.247.77**.

19. The value of the *opcode field* is **2**, which indicates an **ARP reply**.

20. The ethernet address is **00:1e:c1:7e:d9:01**.

21. There are no replies because although my device received the request, my device cannot answer the request, so my device dropped the packet.
