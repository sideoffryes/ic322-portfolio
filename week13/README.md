# Week 13: More Link Layer

## MIDN 2/C Henry Frye, IC322 Fall AY24

## Links to this week's work

[Learning Goals](learningGoals.md)

[ReviewQuestions](reviewQuestions.md)

[Packet Sniffer Lab](lab.md)

[Week 12 Partner Feedback](https://gitlab.com/gabespencer021/ic322-portfolio-2.0/-/issues/24)

## VintMania

1. Vint Cerf co-created the TCP/IP stack and implemented it into one of the first ARPANET nodes.
2. Vint Cerf interned at Rocketdyne after high school and worked on the F-1 rocket engine. Vint Cerf also worked on protocols designed for communications in space, namely to compensate for delays in travel because the speed of light is not quite that fast in regards to space. Vint Cerf served as the first president of the Internet Society, a nonprofit with the mission of "promoting the open development, evolution, and use of the Internet for the benefit of all people throughout the world."
3. How can we possible regulate new technologies like Artificial Intelligence when it seems like new products and innovations are coming out at such a rapid pace? How can we promote the development of new and improved protocols overcome the challenge of implementing in such a rapidly growing internet? These questions are insightful because they deal with two pressing issues, and Vint Cerf would have special insight to these issues due to his work on advisory committees and implementing his own protocols.
