# Learning Goals Answers

1. I can describe the role a switch plays in a computer network.

    A **link-layer switch** is a *self-learning* device that is *transparent* to the hosts and routers on a network and performs important services such as *forwarding* and *filtering*.

    *Filtering* describes the process of deciding if an incoming frame should be forwarded to one of the switch's interfaces or dropped.

    *Forwarding* is the process in which a switch moves an incoming frame to an interface to be further transmitted.

    Both *filtering* and *forwarding* is done with a *switch table*. The table contains entries for nearly all of the hosts and routers on the local network. An entry is made up of a MAC address, an interface that leads to the MAC address, and the time when the entry was made. An important distinction between network-layer switches and link-layer switches is that link-layer switches forward frames based on *MAC addresses* whereas network-layer switches forward packets based on *IP addresses*.

    When a frame arrives at a packet, the *switch table* is indexed with the *MAC addresses* in the incoming frame's destination field. If there is no entry at that index, the frame is *broadcasted* to all interfaces. If there is an entry that matches the frame to the interface it was received on, the frame was discarded. Finally, if the MAC address maps to an interface that is not the interface the frame was received on, it is simply forwarded to that interface.

    A switch is considered to be *self-learning* because every time a frame is received, the switch stores the frame's source address, the interface it arrived on, and the time it was received. If every host on the LAN sends a frame, the switch will be aware of every host on the network and their respective MAC addresses. If no frame is received from a MAC addresses after a particular period of time, the entry is deleted from the table. This ensures that the switch is able to adapt to devices joining and leaving the network. This prevents administrators from needing to manually configure switches.

2. I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.

    The Problem: How do we translate between IP addresses and MAC addresses?

    The Solutions: **Address Resolution Protocol (ARP)**. Each host and router contains an *ARP table* which contains entries made of an *IP address*, a *MAC address*, and a *time to live* that specifies when the entry will expire. If a host is trying to send data to a host doesn't know their MAC address, the sender will construct a special *ARP packet* and broadcast it to all other hosts on the network. Upon reception, if a hosts IP address does not match the destination IP of the ARP packet, it drops the message. If a host has a matching IP, it will respond to the sender with its MAC address. Similar to link-layer switches, this protocol is automatic and does not require configuration. ARP is analogous to DNS.

3. I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.

    **Carrier sense multiple access with collision avoidance (CSMA/CA)** senses the channel before transmitting and will not transmit when the channel is busy, similar to CSMA/CD. CSMA/CA differs, however, because it implements features unique to 802.11 wireless transmissions. Instead of collision *detection*, collision *avoidance* is used in combination with *acknowledgement/retransmission (ARQ)* due to high bit errors. More specifically, wireless transmission cannot easily send and receive at the same time, so it is extremely difficult to *detect* collisions. Once a wireless station begins transmitting, it will not stop until the entire frame has been transmitted. Wireless stations also implement *inter-frame spacing* delays to sense the channel before sending. When a frame is initially going to be send, the sender will delay during the *distributed inter-frame space* to detect if the channel is idle. If it is *not* idle, the station will wait for a random amount of time until the channel is idle before transmitting. Upon reception, the receiver will delay during the *short inter-frame spacing* to sense if the channel is idle. Once the channel is idle, the receiver will send an acknowledgement. If the ARQ is received, the sender will know the transmission was successful. If it is not received, the sender will retransmit following the same process.
