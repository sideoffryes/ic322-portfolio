# Review Question Answers

## R23 Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values

To answer this question, I used the windows command prompt and ran the command *ipconfig*.

1. IP address: 10.16.51.207
2. Network Mask: 255.255.254.0
3. Default Router: 10.16.50.1
4. Address of local DNS server: none listed

## R26 Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?

The router uses *DHCP* to automatically assign each of the PCs a local/*private* IP address. If the router is using *IPv4*, it will use *NAT* because there are not enough *public* IP addresses in *IPv4* to assign each host its own *public* IP address. If the router is using *IPv6*, then each device will get its own *public IPv6* address instead of a *private* IP address, and the router will not need to use *NAT*.

## R29 What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain

A *private network address* is an address which is only relevant in the scope of the local subnet. They are frequently used in subnets that implement *NAT*.

There should never be a datagram with a *private* address in the public internet, because the router should have used the *NAT table* to translate the *private* address to the *public* address of the router.

## P15 Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F

### P15a Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; Subnet B should have enough addresses to support 120 interfaces; and Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x – e.f.g.h/y

Address Range: 214.97.245/23 = 214.97.1111111*.********

| Subnet    | Number of Interfaces  |     Addresses     |      Address (Binary)    |
| :-------: | :-------------------: | :---------------: | :----------------------: |
|   A       | 250                   | 214.97.254.0/24   | 214.97.11111110.0        |
|   B       | 120                   | 214.97.254.250/24 | 214.97.11111110.11111010 |
|   C       | 120                   | 214.97.255.115/23 | 214.97.11111111.01110011 |
|   D       | 2                     | 214.97.255.235/23 | 214.97.11111111.11101011 |
|   E       | 2                     | 214.97.255.237/23 | 214.97.11111111.11101101 |
|   F       | 2                     | 214.97.255.239/23 | 214.97.11111111.11101111 |

### P15b Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers

## P16 Use the whois service at the American Registry for Internet Numbers (http://www.arin.net/whois) to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities

No, whois cannot be used to determine with certainty the geographical location of a specific IP address because it returns the records associated with the domain that were provided when the domain was registered. In the case of the university I looked up, servers seemed to belong to Amazon, and the addresses that were provided were Amazon office addresses, not the address of the specific server that I looked up.

## P18 Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.101.225 and that the network address of the home network is 192.168.0/24

### P18a Assign addresses to all interfaces in the home network

1. 192.168.0.1
2. 192.168.0.2
3. 192.168.0.3

### P18b Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table

|     Internal     |      External      |
| :--------------: | :----------------: |
| 192.168.0.1/4000 | 24.34.101.225/7000 |
| 192.168.0.1/4001 | 24.34.101.225/7001 |
| 192.168.0.2/4002 | 24.34.101.225/7002 |
| 192.168.0.2/4003 | 24.34.101.225/7003 |
| 192.168.0.3/4004 | 24.34.101.225/7004 |
| 192.168.0.3/4005 | 24.34.101.225/7005 |

## P19 Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world

### P19a Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer

In short, you would attempt to create groups of numbers that seemingly start at a number then are each separated by 1. For example, if a host started with an ID number of 62000, you would make a group with all of the ID numbers that are contiguous to 62000.

### P19b If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer

No because the technique relies on all of the packets from the same source being sequentially numbered.
