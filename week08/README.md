# Week 08: More Network Layer: More Data Plane

## MIDN 2/C Henry Frye, IC322 Fall AY24

This week, we continued our discussion on the network layer and the data plane.

## Links to this week's work

[Learning Goals](learningGoals.md)

[Review Questions](reviewQuestions.md)

[Beta Testing Protocol Pioneer pt. 2](lab.md)

[Week 07 Peer Feedback](https://gitlab.usna.edu/m257074/ic322-portfolio/-/issues/14)
