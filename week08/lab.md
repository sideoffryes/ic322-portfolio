# Beta Testing Protocol Pioneer pt. 2

## Introduction

This lab involved beta testing the "Protocol Pioneer" game created by LCDR Downs by playing through the third and fourth chapters to learn about how routing messages and routing tables work.

## Collaboration

I did not collaborate with anyone on this lab.

## Process

I followed all instructions on the course page as well as the gitlab repo.

## Lab Questions

1. How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

    To solve chapter 1, I wrote a few lines to first parse the message that the drone had received by splitting the message by new line characters and colons. After that, I checked if we were at the target drone. If we were, then we could go ahead and execute the command. If not, I used a ton of if statements to figure out where we needed to send the message next based on the drone it was currently at. This was the code for my solution:

    ```python
        msg_parts = m.text.split("\n")
        dest = int(msg_parts[0].split(':')[1])
        cmd = msg_parts[1].split(':')[1]
        val = msg_parts[2].split(':')[1]

        if int(self.id) == dest:
            if cmd == "start_emit":
                self.start_emit(val)
            elif cmd == "focus":
                self.focus(val)
            elif cmd == "return_results":
                self.return_results(val)
        else:
            dir = ""
            if self.id == "1":
                if dest == 3:
                    dir = "N"
                elif dest == 2:
                    dir = "E"
                else:
                    dir = "E"
            elif self.id == "2":
                if dest == 5:
                    dir = "S"
                elif dest == 1:
                    dir = "W"
                else:
                    dir = "N"
            elif self.id == "3":
                if dest == 1:
                    dir = "S"
                else:
                    dir = "E"
            elif self.id == "4":
                if dest == 6:
                    dir = "N"
                elif dest == 2 or dest == 5:
                    dir = "S"
                else:
                    dir = "W"
            elif self.id == "5":
                dir = "N"
            elif self.id == "6":
                dir = "S"
            
            if dir != "":
                self.send_message(m.text, dir)
    ```

    For chapter 2, I basically repeated my solution to part 1. When I parsed the message, I did some extra parsing to split the destination address into its type and identifier to use later in my makeshift routing table. After I had parsed the message, I wrote the router/drone code by writing a unique set of if statements per drone. In each block, I wrote separate conditions for if the target was an analyzer, scanner, or another drone. In each of those smaller blocks, I wrote specific routing conditionals based on the location of the specific drone. I then repeated that for every drone. The code for the scanners was simpler; I began by paring the message and executing the command from the message. I then created a new message where I filled in the output from the command and set the destination of the *new* message to the original value of the message source. I then wrote a small conditional to account for the different locations of the two scanners. This is the code for my solution:

    ```python
        def drone_strategy(self):
            """Drones are responsible for routing messages."""
            while(self.message_queue):
                m = self.message_queue.pop()
                print(f"--- Drone {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")

                # Process message
                msg_parts = m.text.split("\n")
                source = msg_parts[0].split(':')[1]
                dest = msg_parts[1].split(':')[1]
                command = msg_parts[2].split(':')[1]
                value = msg_parts[3].split(':')[1]

                # Process source
                sourceType = int(source.split('.')[0])
                sourceId = int(source.split('.')[1])

                # Process destination
                destType = int(dest.split('.')[0])
                destId = int(dest.split('.')[1])

                # Begin routing
                dir = ""
                # Drone 3.1
                if self.id == "3.1":
                    # Analyzers
                    if destType == 1:
                        if destId == 1:
                            dir = "W"
                        else:
                            dir = "S"
                    # Scanners
                    elif destType == 2:
                        dir = "E"
                    # Other drones
                    else:
                        if destId == 7 or destId == 6 or destId == 5:
                            dir = "E"
                        else:
                            dir = "S"
                # Drone 3.2
                elif self.id == "3.2":
                    # Analyzers
                    if destType == 1:
                        if destId == 1:
                            dir = "N"
                        elif destId == 2:
                            dir = "W"
                        else:
                            dir = "S"
                    # Scanners
                    elif destType == 2:
                        dir = "E"
                    # Other drones
                    else:
                        if destId == 7 or destId == 6 or destId == 5:
                            dir = "E"
                        elif destId == 1:
                            dir = "N"
                        else:
                            dir = "S"
                # Drone 3.3
                elif self.id == "3.3":
                    # Analyzers
                    if destType == 1:
                        if destId == 3:
                            dir = "S"
                        else:
                            dir = "N"
                    # Scanners
                    elif destType == 2:
                        dir = "E"
                    # Other drones
                    else:
                        if destId == 7 or destId == 6 or destId == 5:
                            dir = "E"
                        elif destId == 1:
                            dir = "N"
                        else:
                            dir = "S"
                # Drone 3.4
                elif self.id == "3.4":
                    # Analyzers
                    if destType == 1:
                        if destId == 3:
                            dir = "W"
                        else:
                            dir = "N"
                    # Scanners
                    elif destType == 2:
                        dir = "N"
                    # Other drones
                    else:
                        dir = "N"
                # Drone 3.5
                elif self.id == "3.5":
                    # Analyzers
                    if destType == 1:
                        dir = "W"
                    # Scanners
                    elif destType == 2:
                        if destId == 1:
                            dir = "N"
                        else:
                            dir = "S"
                    # Other drones
                    else:
                        if destId == 3 or dest == 4:
                            dir = "W"
                        else:
                            dir = "N"
                # Drone 3.6
                elif self.id == "3.6":
                    # Analyzers
                    if destType == 1:
                        dir = "W"
                    # Scanners
                    elif destType == 2:
                        if destId == 1:
                            dir = "N"
                        else:
                            dir = "S"
                    # Other drones
                    else:
                        if destId == 7:
                            dir = "N"
                        elif destId == 5:
                            dir = "S"
                        else:
                            dir = "W"
                # Drone 3.7        
                else:
                    # Analyzers
                    if destType == 1:
                        dir = "W"
                    # Scanners
                    elif destType == 2:
                        if destId == 1:
                            dir = "N"
                        else:
                            dir = "S"
                    # Other drones
                    else:
                        if destId == 6 or dest == 5:
                            dir = "S"
                        else:
                            dir = "W"
                if dir != "":
                        self.send_message(m.text, dir)

        def scanner_strategy(self):
            """Scanners are responsible for receiving messages, parsing them, taking
            action, and responding with results."""
            while(self.message_queue):
                m = self.message_queue.pop()
                print(f"--- Scanner {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")

                # Process message
                msg_parts = m.text.split("\n")
                source = msg_parts[0].split(':')[1]
                dest = msg_parts[1].split(':')[1]
                command = msg_parts[2].split(':')[1]
                value = int(msg_parts[3].split(':')[1])
            
                # Process source
                sourceType = int(source.split('.')[0])
                sourceId = int(source.split('.')[1])
            
                # Process destination
                destType = int(dest.split('.')[0])
                destId = int(dest.split('.')[1])

                if command == "boot":
                    result = self.boot(value)
                elif command == "aim":
                    result = self.aim(value)
                else:
                    result = self.scan(value)

                newMsg = "Source:" + self.id + "\nDest:" + str(source) + "\nCommand:Result\nValue:" + str(result)
                if self.id == "2.1":
                    dir = "S"
                else:
                    dir = "N"
                self.send_message(newMsg, dir)
    ```

2. Include a section on Beta Testing Notes.

    My main issue with these two chapters, particularly the fourth chapter, was that it felt like I was playing "if statement simulator." I have a sneaking suspicion that I may have not produced the desired solution based on the high volume of if statements that I wrote, but the method I used seemed like the most obvious based on my knowledge of python. These led to a pretty dull experience of copying and pasting the same code over and over again and changing the numbers around a little bit.

    In addition, there are times where it feels like the lab is testing me more on my python syntax than it is on my knowledge of networks. I frequently have type casting issues where I do not know the type (typically a string or an int) that the lab is expecting me to send something as, and due to the nature of python being an interpreted language, I do not find out that I did it wrong until the code is already running. I then fix the code but then have to wait a while for the game to run to reach the failure point again.
