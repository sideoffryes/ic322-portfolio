# Week 10 Lab

## Loopbacker: Week 08 NAT Wireshark Lab

## Introduction

This lab uses Wireshark to examine how NAT works bother internally and externally to a network.

## Collaboration

I did not collaborate with anyone on this lab.

## Process

I followed all of the instructions in the directions from the textbook and used the included wireshark files from the instructions.

## Lab Questions

1. The source IP address is **192.168.10.11**. The source port number is **53924**. The destination IP address is **138.76.29.8**. The destination port number is **80**.

2. The HTTP 200 OK message is received at time **0.030672101**.

3. The source IP address is **138.76.29.8**, and the destination IP address is **192.168.10.11**. The source port is **80**, and the destination port is **53924**.

4. It appeared at time **0.027356291**.

5. The source IP address is **10.0.1.254**, and the destination IP address is **138.76.29.8**. The source port is **53294**, and the destination port is **80**.

6. The **source IP address** is different.

7. **No.**

8. **Checksum** is changed because the source IP address is changed.

9. It appears at time **0.030625966**.

10. The source IP and port is **138.76.29.8 and port 80**. The destination IP and port is **10.0.1.254 and port 53294**.

11. The source IP and Port is **138.76.29.8 and 80**. The destination IP and port is **192.168.10.11 and 80**
