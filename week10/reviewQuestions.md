# Review Question Answers

## R11 How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?

BGP uses NEXT-HOP to determine where to route traffic. It is also used when choosing between multiple paths. In this situation, the cost to get to the NEXT-HOP of each path options is analyzed in combination with other factors.

The AS-PATH attribute lists which ASs a message with need to go through to get to its destination.

## R13 True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain

False. It will only do that if the router is a border router. The border router will then add its identity before sending it to its neighbors. If it is an internal router that received the message, it does not need to add its identity because all of its neighbors are all in the same AS.

## R19 Names four different types of ICMP messages

- Type 0 Code 0: echo reply(to ping)
- Type 3 Code 0: destination network unreachable
- Type 3 Code 1: destination host unreachable
- Type 3 Code 2: destination protocol unreachable

## R20 What two types of ICMP messages are received at the sending host executing the Traceroute program?

- Type 11 Code 0: Once the TTL on the packet has expired, the router drops the datagram and sends an ICMP message back to the source containing the name of the router and its IP address.

- Type 3 Code 3: The destination host will send back an ICMP message indicating that the port number specified in the UDP packet is unreachable, indicating that the program has finally reached the destination and does not need to send any more packets.

## P14 Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4

![P14 Diagram from the textbook](Week10P14.png)

### P14a Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?

eBGP. The router received an advertisement for prefix x from an external AS.

### P14b Router 3a learns about x from which routing protocol?

iBGP. The only way for router 3a to learn about prefix x is from router 3b, which is internal to its AS.

### P14b Router 1c learns about x from which routing protocol?

eBGP. The router received an advertisement for prefix x from an external AS.

### P14b Router 1d learns about x from which routing protocol?

iBGP. The only way for router 1d to learn about prefix x is from another router in its AS.

## P15 Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table

### P15a Will I be equal to I1 or I2 for this entry? Explain why in one sentence

I1 because it provides the shortest path to the NEXT-HOP of the path.

### P15b Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I1 or I2? Explain why in one sentence

I2 because the distance to NEXT-HOP is shorter than going through I1.

### P15c Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I1 or I2? Explain why in one sentence

I2. AS1 doesn't care about how long it will take the message to get through the other ASs, it only cares about getting the message to the closes NEXT-HOP, which will be through I2.

## P19 In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?

![Figure 5.13](Week10P19.png)

A would only advertise a path to W to B, but it would advertise a path to V to both B and C.

C would receive route A V.

## P20 Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?

No. It will appear to Z as if all traffic is coming from Y without being able to differentiate traffic coming from X. The administrators of Z would have to form an agreement with the administrators of Y to not forward any traffic from X to Z.
