# Learning Goals Answers

1. I can explain what autonomous systems are and their significance to the Internet.

    Autonomous systems (ASs) are networks of routers that have been grouped together for administrative purposes. In an AS, different routing algorithms are used to route internal traffic than are used for external traffic. As mentioned in Week 09, OSPF is a common example of an intra-AS routing protocol. It is critical that ASs can communicate with other ASs, so they use external routing protocols like Border Gateway Protocol (BGP). ASs are identifiable by a unique Autonomous System Number (ASN), assigned by ICANN regional registries. The internet can be viewed as many ASs that communicate with each other to exchange hosts's traffic, like ISPs. This allows an ISP/network owner to administer their network however they would like and hide the internal organization and other aspects of their network from other networks while still being able to communicate with them.

2. I can describe how the BGP protocol works as well as why and where it is used.

    The Border Gateway Protocol (BGP) is one of the backbone protocols of the internet, along with IP. All ASs run BGP in order to communicate with each other, and therefore, GBP glues all of the ISPs together.

    BGP does not focus on specific addresses, rather it focuses on routing based on prefixes representing a singular subnet or a collection of subnets. This provides each router with the capability to advertise its subnet's existence to all other routers, and to determine the best route to get to another router.

    BGP advertises information by first stating that a subnet exists then passing the route to get to that subnet to all of an AS's neighbors. For example, an AS, AS1, will state that it contains a subnet, x, to its neighbor, AS2. AS2 will then tell its neighbor, AS3, that subnet x exists, and AS3 can reach it by going through AS2 to AS1. This processes repeats over and over until each interconnected AS knows a route to get to the subnet. These messages are exchanges by routers through semi-permanent TCP connections, often called a BGP connection. Internal BGP (iBGP) connections and external BGP (eBGP) connections are used to pass messages through ASs.

    The shortest BGP route is typically calculated using the NEXT-HOP attribute, the IP address of the router the begins a path through an external AS. A simple routing method, Hot Potato Routing, routes traffic by sending it along the least costly path to the NEXT-HOP router. For example, if there are two different routes through two different ASs to get to the target subnet, Hot Potato Routing will choose whichever route has a lower cost to get to the NEXT-HOP router, regardless of the cost of the path once it is in the other AS. This is considered to be a *selfish* protocol, because an AS only considers its own cost and does not consider the cost to another AS to route the message(s). The actual route selection algorithm is more complicated but follows a similar principle of minimizing costs based on a network administrator's preferences and path length and cost.

3. I can explain what the ICMP protocol is used for, with concrete examples.

    Internet Control Message Protocol (ICMP) is used by both hosts and routers, mainly for error reporting. The ICMP protocol is considered to be a part of the network layer, but it behaves more like a link layer protocol in the sense that IP packets carry ICMP messages. An ICMP message contains a code field that corresponds with the reason the packet was generated.

    An example of a program that uses ICMP is the ping program. The ping program sends ICMP messages with type 8 code 0 messages, indicating an echo request.

    The Traceroute program also uses ICMP by sending messages that are type 11 code 0 to transport the name of a router and its IP address.

    ![ICMP Types and Codes](Week10LO3.png)
