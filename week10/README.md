# Week 10: Network Layer: More Control Plane

## MIDN 2/C Henry Frye, IC322 Fall AY24

## Links to this week's work

[Learning Goals](learningGoals.md)

[Review Questions](reviewQuestions.md)

[Loopbacker: Week 08 Wireshark Lab](lab.md)

[Week 09 Partner Feedback](https://gitlab.com/soleilxie1/ic322-portfolio/-/issues/17)
