# Week 01: The Internet

*MIDN 2/C Henry Frye, IC322 Fall AY24*

This week, we were introduced to a basic overview of how the internet works and some of the infrastructure that it is built on. We discussed hardware like routers and switches. We also began talking about protocols like IP, HTTP, and TLS.

## Links to this week's work

[Into to Wireshark Lab](wiresharkLab.md)

[Learning Goals](learningGoals.md)

[Review Questions](questions.md)
