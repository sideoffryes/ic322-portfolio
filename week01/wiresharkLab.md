# Intro to Wireshark Lab

## Introduction

This lab was a brief introduction to Wireshark that involved analyzing a small capture of web traffic and following some steps to determine information about the traffic we captured.

## Collaboration

I collaborated with MIDN 2/C Koutrakos by providing him with assistance to set up his Wireshark environment and pointed out information in some of the packets. I also assisted MIDN 2/C Angulo with question 1.

## Process

I completed the "Getting Started" lab about Wireshark found in the textbook, version 8.1. I was unable to capture live traffic on my lab computer, so I used a .pcap file that was provided in the footnotes of the lab. Besides capturing traffic, I followed all instructions from the textbook.

## Lab Questions

1. The protocols I found were:
    - ARP
    - HTTP
    - ICMPv6
    - STP
    - TCP
    - TLSv1.2
2. It took 0.028885 seconds for the reply to be received.
    > 53.940406 - 53.911521 = 0.028885
3. The Internet address of the gaia.cs.umass.edu server is: **128.119.245.12** The Internet address of the computer that sent the request in the capture file is **10.0.0.44**
4. The browser that sent the request was **Firefox**

    The full User-Agent string was: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 Firefox/84.0`

5. The destination port is port **80**

6. This is the printout of the two HTTP GET and OK messages.
![Question 6 Packet Capture Image](<question6.jpg>)
