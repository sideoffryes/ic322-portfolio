# Review Question Answers

#### I collaborated with MIDN 2/C Caleb Koutrakos

### What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?

They are different ways of referring to the same thing. A host refers to a system with the context of running an application on it, whereas an end system refers to a system by where it falls in a network hierarchy. Examples of end systems could be an Amazon Alexa, an XBox, a smart TV, or an automobile connected to the internet. A web server would be considered an end system.

### List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

- DSL - home access
- HFC - home access
- Fiber - enterprise
- Satellite - wide-area wireless

### Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)

`end-to-end delay = N(processing delay + transmitting delay + propagation delay)`
$$
d_{end-to-end} = N(processing \: delay + transmitting \: delay + propagation \: delay)
$$
$$
d_{end-to-end} = transmitting \: delay
$$
$$
d_{end-to-end} = {L1 \over R1} + {L \over R2}
$$

### What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

A circuit-switch network supplies constant internet access without having to share your bandwidth.

A TDM fairly distributes bandwidth because each packet will get the full bandwidth, and there are no collisions with other data.

### Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. (See the discussion of statistical multiplexing in Section 1.3.)

#### When circuit switching is used, how many users can be supported?

$$
{2 \: Mbps \over 1 \: Mbps} = 2 \: users
$$

#### For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?

There will be no delay because even if both users are sending data at the same time, there will only be a maximum of 2 Mbps of data traveling to the link, and the link is able to transmit 2 Mbps onto the wire, so no packets will ever have to wait in the buffer to be transmitted. A queuing delay will begin if any more users start to transmit, because the link will no longer be able to transmit data as fast as it is receiving data from the users.

#### Find the probability that a given user is transmitting

20% (given in the question).

#### Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows

$$
0.2*0.2*0.2 = 0.008 = 0.8\%
$$
The queue will only grow when three users are transmitting, so ${8 \over 1000}$ of the time.

### Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

ISPs will peer with each other so that they can transmit data to its destination without having to pay a fee to a Tier 1 ISP.

IXPs provide a point for ISPs to peer together, and it makes it simpler such that the ISPs only have to form an agreement with the IXP rather than all of the other ISPs.

### A user can directly connect to a server through either long-range wireless or a twisted-pair cable for transmitting a 1500-bytes file. The transmission rates of the wireless and wired media are 2 and 100 Mbps, respectively. Assume that the propagation speed in air is 3 * 108 m/s, while the speed in the twisted pair is 2 * 108 m/s. If the user is located 1 km away from the server, what is the nodal delay when using each of the two technologies?

$$
d_{nodal} = d_{proc} + d_{trans} + d_{prop} \\ \\
d_{nodal \: wireless} = {1500 \over 2 \: Mbps} + {1000 \: m \over 3 * 108 \: m/s} = 753 \: sec \\
d_{nodal \: wired} = {1500 \over 100 \: Mbps} + {1000 \: m \over 2*108 \: m/s} = 19.63 \: sec
$$

### Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps

#### Assuming no other traffic in the network, what is the throughput for the file transfer?

500 kbps, because it is the bottleneck link.

#### Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?

$$
{4000000 \: bytes \over 500000 \: bps} = 8 \: sec
$$

#### Repeat (a) and (b), but now with R2 reduced to 100 kbps

The new throughput is now 100 kbps because R2 is now the bottleneck link.
$$
{4000000 \: bytes \over 1000000 \: bps} = 40 \: sec
$$
