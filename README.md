# MIDN Henry Frye IC322 Portfolio

This portfolio contains all of my course work for IC322

[Week 1 - The Internet](week01/)

[Week 2 - The Application Later: HTTP](week02/)

[Week 3 - The Application Layer: DNS and OTher Protocols](week03/)

[Week 4 - The Transport Layer: TCP and UDP](week04/)

[Week 5 - The Transport Layer: Part 2](week05/)

[Week 7 - Network Layer: Data Plane](week07/)

[Week 8 - Network Layer: More Data Plane](week08/)

[Week 9 - Network Layer: Control Plane](week09/)

[Week 10 - Network Layer: More Control Plane](week10/)

[Week 12 - Link Layer](week12/)

[Week 13 - More Link Layer](week13/)

[Week 15 - TLS and Network Security](week15/)
