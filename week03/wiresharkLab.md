# Wireshark DNS Lab

## Introduction

This lab used wireshark to analyze DNS and HTTP packets to illustrate how DNS works in a real use case.

## Collaboration

I collaborated with MIDN 2/C Caleb Koutrakos.

## Process

I completed the DNS lab from the textbook website, version 8.1. I used the .pcap files that were available from the website to analyze in the lab. I followed all of the instructions in the lab.

## Lab Questions

1. The IP address of `www.iitb.ac.in` is `103.21.124.10`.
2. The DNS server's IP is `127.0.0.53`.
3. The answer came from a non-authoritative server, because the response to my `nslookup` command includes "Non-authoritative answer"
4. There are 3 authoritative DNS servers:
    1. `dns1.iitb.ac.in`
    2. `dns2.iitb.ac.in`
    3. `dns3.iitb.ac.in`
5. The packet number of the DNS query message is `packet number 15`. It was sent over `UDP`. The packet contains a a "User Datagram Protocol" section in its details.
6. The DNS response message is `packet number 17`. It is also `UDP`.
7. The destination port of the DNS query message is `port 53`. The source port of the DNS response message is also `port 53`. The DNS service operates on port 53.
8. The destination IP address is `75.75.75.75`
9. The DNS query contains `1 question` and `0 answers`.
10. The DNS response contains `1 questions` and `1 answer`
11. The initial HTTP GET request for the base file is `packet number 22`. The initial DNS query to resolve the domain is `packet number 15`. The DNS response is `packet number 17`. The HTTP GET request for the image object is `packet number 205`. The DNS query that allows the second HTTP request is still `packet number 17`. The location of the domain is already stored in the DNS cache, so the local DNS server does not need to make another request to find out the IP of the intended host, it can just use the same answer as before.
12. The `the destination port for the query and the source for the response are both port 53`.
13. It's sent to IP address `75.75.75.75`
14. The query message is `Type-A`
15. The DNS response contains `1 Question and 1 Answer`
16. The destination IP address is `127.0.0.53`. This is my local DNS server as USNA.
17. There is `1 question and 0 answers` in the query.
18. There are `3 answers` in the response. The answers contain different name servers for the same domain. There are `3 additional resource records returned`. The `additional resource records provide the IP addresses to the DNS servers in the answers section`.
