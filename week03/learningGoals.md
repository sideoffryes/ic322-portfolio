# Learning Goal Answers

1. I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.

    - *Local DNS Servers* are not strictly in the DNS hierarchy, but they are used by ISPs to collect and forward DNS requests to the DNS hierarchy. The benefit of having a local DNS server is that if the local DNS server has the desired hostname/IP address combination cached, it can quickly return it to the client instead of having to forward the request to the root DNS server.

    - *Authoritative DNS Servers* are a point-of-contact for the public to connect to in order to access the DNS records of all of an institutions sub-domains. For example, USNA might have an authoritative DNS server that contains all of the DNS records for all hostnames ending in *.usna.edu*. This provides access to an institution's publicly available services such as web and mail servers.

    - *Top-Level-Domain DNS Servers* contain the DNS records for hostnames ending in each of the top-level-domains. Some examples are: *.com*, *.net*, *.edu*, *.gov*, *.ca*, etc. The organization that owns the DNS servers for the *.com* TLD contains all of the DNS recorders that point to all hosts ending in *.com*.

    - *Root DNS Servers* provide the IP addresses for all of the TLD servers.

    If we are ignoring DNS caching, a typical DNS requests would first get forwarded to a *root server*. The *root server* would then return the IP address to the appropriate *TLD server*. The *TLD server* would then examine the request and return the IP address to the appropriate *authoritative DNS* server. The *authoritative DNS* server would then return the IP address to the server that the client wants to access.

2. I can explain the role of each DNS record type.

    DNS records are referred to as *resource records*. They contain four fields: *Name*, *Value*, *Type*, and *TTL*. which can have 4 different types:

    1. Type=A records contain a hostname and IP address pair. These make up the majority of resource records.
    2. Type=NS records provide a domain name and authoritative DNS server pair. This helps DNS queries move along the DNS hierarchy.
    3. Type=CNAME records provide the canonical hostnames for alias hostnames.  
    4. Type=MX records provide the canonical name for the mail server corresponding to the hostname. This allows mail servers to have simple aliases that match the domain name.

3. I can explain the role of the SMTP, IMAP, and POP protocols in the email system.

    - **SMTP** - The *Simple Mail Transfer Protocol* provides the backbone of sending electronic mail. It provides a standard format that should be used so that messages can be sent electronically. It uses TCP to messages from from the sender's mail server directly to the recipient's mail server. This protocol is only for *sending* mail. The protocol can be configured to continue to attempt to send a user a message if their mail server isn't responding.
    - **IMAP** - *Internet Mail Access Protocol* creates a system of message boxes on mail servers where users can request the server to send them any mail that they have waiting for them.
    - **POP** - The *Post Office Protocol* provides one-way synchronization. This only allows a server to push mail to a client instead of the client pulling the mail from the server to the client. Once the email has been pushed from the server to a particular client, the mail is deleted and cannot be accessed from any other client devices.

4. I know what each of the following tools are used for: `nslookup`, `dig`, `whois`.

    - `nslookup` is a tool that allows users to query information about a given domain, including its IP address.

    - `dig` is a tool which returns the responses from each DNS server that a DNS message passes through. It is commonly used to see the path that a DNS message takes to arrive at its destination.

    - `whois` returns the registration information associated with a given domain.
