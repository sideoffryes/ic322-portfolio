# Review Question Answers

### R16 Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.

When Alice sends a message from a web-based e-mail account, it will be transmitted to her email server using HTTP. When Alice's server send the email to Bob's mail server, it will use SMTP. SMTP is more universally accepted for sending mail, and it provides the capability of sending the email multiple times if Bob's server does not respond. Once the message reaches Bob's mail server, Bob can retrieve the message using the IMAP protocol.

### R18 What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?

In *HTTP/1.1* bytes from an HTTP message must be delivered *in-order*. If they are not, the remaining bytes from the request cannot be sent until the lost bytes are resent. *HTTP/2* uses framing, a method where messages are broken down into independent smaller frames that can be interwoven/sent out of order and reassembled, so they do not need to be delivered in order. They are also encoded using binary, so they are easier to pare and produce less parse errors.

### R24 CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them

- **Enter Deep** - server clusters are deployed in access ISPs around the world so that the CND servers are as close to the end user as possible. This is intended to reduce the perceived delay and decreasing the number of hops between the client and the CDN. This becomes a challenging strategy because it is highly-distributed, and it become difficult to manage and maintain all of the CDN clusters.

- **Bring Home** - server clusters are larger but are placed at fewer ISP sites. Instead of being places at access ISPs, these clusters would be at IXPs. This layout provides the inverse of the *enter deep* strategy; perceived delay may increase and the number of steps to get from the user to the content may increase, but the clusters will be much easier to manage and maintain.

### P16 How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain

- **SMTP** - messages are ended with a single period on a line.
- **HTTP** - messages use a *content-length* field to determine when the message is over.
  - *HTTP* cannot use the same method. It is entirely possible that the entity body field could contain a period on a line by itself. At this point, the message would be prematurely cut short without delivering all of the content.

### P18a What is a whois database?

A **whois database** is a database that contains information about domains and the information that was collected when the domain was registered. This can include contact information lines names, addresses, phone numbers, and other identifying information.

### P18b Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used

- Database: `who.is`

1. NS-A.USNA.EDU
2. NS-B.USNA.EDU

### P20 Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the webservers (outside your department) that are most popular among the users in your department? Explain

If you can access the DNS cache, you can time how long each DNS record spends in the cache. If a record stays in the cache for a long time, it means that the webserver is popular and many users are attempting to access it, so it stays in the cache. If a website is not popular, it will quickly be evicted from the cache to make room for more popular websites.

### P21 Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain

Yes. When you run the `dig` command, it also returns the response time of the command. If the response time is greater than 0, the DNS has to forward your request to the higher-level DNS servers because no one else has visited that server yet. If the response time is 0 ms, that means that someone else has visited the website, and it is in the cache of the local DNS server.
