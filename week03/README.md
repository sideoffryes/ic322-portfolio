# Week 03: DNS and Other Protocols

*MIDN 2/C Henry Frye, IC322 Fall AY24*

This week, we discussed Domain Name System (DNS). DNS provides a valuable services to computer networks as a service which can resolve hostnames that humans can understand to IP addresses that computers and computer networks can understand. DNS uses a hierarchy similar to ISPs to provide this service to computer networks, both at a local and international level.

## Links to this week's work

[Wireshark DNS Packet Lab](wiresharkLab.md)

[Learning Goals](learningGoals.md)

[Review Questions](questions.md)
