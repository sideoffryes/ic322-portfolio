# Week 09: Network Layer: Control Plane

## MIDN 2/C Henry Frye, IC322 Fall AY24

This week, we continued to talk about the network layer, but now we switch focus from the data plane to the control plane.

## Links to this week's work

[Learning Goals](learningGoals.md)

[Review Questions](reviewQuestions.md)

[Loopbacker: Week 07 Wireshark Lab](lab.md)

[Week 08 Peer Feedback](https://github.com/chazzconi25/IC322-Portfolio/issues/12)
