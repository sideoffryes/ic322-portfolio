# Review Question Answers

## R5 What is the “count to infinity” problem in distance vector routing?

The "count to infinity" problem describes a situation where a loop forms between two routers as a result of one router believing that it can reach a router through a node that is no longer connected to a destination. This occurs when a router does not yet have updated routing information and believes that it can still reach the disconnected router. The router then enters an infinite loop with the intermediary router where they each begin increasing the cost it will take to get to the disconnected router.

## R8 True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain

False. in OSPF, routers broadcast their routing information across the network to all other routers.

## R9 What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?

The term *area* refers to the ability to construct a hierarchy of OSPF algorithms within a sign *autonomous system*. In this set up, there are one or more border routers that route packets outside of the area and one specific area responsible for acting as the "backbone," which contains all of the border routers and is responsible for routing packets to all other areas in the *AS*. This concept was introduced to be able to divide enormous *Autonomous systems* into smaller, more manageable, segments. 

## P3 Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1

![P3 Diagram](Week09P3.png)

| step | N'      | *D(z)*, *p(z)* | *D(y)*, *p(y)* | *D(v)*, *p(v)* | *D(w)*, *p(w)* | *D(t)*, *p(t)* | *D(u)*, *p(u)* |
| :-:  | :-:     | :-:            | :-:            | :-:            | :-:            | :-:            | :-:            |
| 0    | x       | 8, x           | 6, x           | 3, x           | 6, x           | ∞              | ∞              |
| 1    | xv      | 8, x           | 6, x           |                | 6, x           | 7, v           | 6, u           |
| 2    | xvy     | 8, x           |                |                | 6, x           | 7, v           | 6, u           |
| 3    | xvyw    | 8, x           |                |                |                | 7, v           | 6, u           |
| 4    | xvywu   | 8, x           |                |                |                | 7, v           |                |
| 5    | xvywut  | 8, x           |                |                |                |                |                |
| 6    | xvywutz |                |                |                |                |                |                |

## P4 Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following

### P4c Compute the shortest path from v to all network nodes

| step | N'      | *D(z)*, *p(z)* | *D(y)*, *p(y)* | *D(x)*, *p(x)* | *D(w)*, *p(w)* | *D(t)*, *p(t)* | *D(u)*, *p(u)* |
| :-:  | :-:     | :-:            | :-:            | :-:            | :-:            | :-:            | :-:            |
| 0    | v       | ∞              | 8, v           | 3, v           | 4, v           | 4, v           | 3, v           |
| 1    | vx      | 11, x          | 8, v           |                | 4, v           | 4, v           | 3, v           |
| 2    | vxu     | 11, x          | 7, v           |                | 4, v           | 4, v           |                |
| 3    | vxut    | 11,x           | 7, v           |                | 4, v           |                |                |
| 4    | vxutw   | 11, x          | 7, v           |                |                |                |                |
| 5    | vxutwy  | 11, x          |                |                |                |                |                |
| 6    | vxutyz  |                |                |                |                |                |                |

### P4d Compute the shortest path from w to all network nodes

| step | N'      | *D(z)*, *p(z)* | *D(y)*, *p(y)* | *D(x)*, *p(x)* | *D(v)*, *p(v)* | *D(t)*, *p(t)* | *D(u)*, *p(u)* |
| :-:  | :-:     | :-:            | :-:            | :-:            | :-:            | :-:            | :-:            |
| 0    | w       | ∞              | ∞              | 6, w           | 4, w           | ∞              | 3, w           |
| 1    | wu      | ∞              | ∞              | 6, w           | 4, w           | 5, u           |                |
| 2    | wuv     | ∞              | 12, v          | 6, w           |                | 5, u           |                |
| 3    | wuvt    | ∞              | 12, v          | 6, w           |                |                |                |
| 4    | wuvtx   | 14, x          | 12, v          |                |                |                |                |
| 5    | wuvtxy  | 14, x          |                |                |                |                |                |
| 6    | wuctxyz |                |                |                |                |                |                |

### P4e Compute the shortest path from y to all network nodes

| step | N'      | *D(z)*, *p(z)* | *D(w)*, *p(w)* | *D(x)*, *p(x)* | *D(v)*, *p(v)* | *D(t)*, *p(t)* | *D(u)*, *p(u)* |
| :-:  | :-:     | :-:            | :-:            | :-:            | :-:            | :-:            | :-:            |
| 0    | y       | 12, v          | ∞              | 6, w           | 8, y           | 7, y           | ∞              |
| 1    | yx      | 12, v          | 12, x          |                | 8, y           | 7, y           | ∞              |
| 2    | yxt     | 12, v          | 12, x          |                | 8, y           |                | 9, t           |
| 3    | yxtv    | 12, v          | 12, x          |                |                |                | 9, t           |
| 4    | yxtvu   | 12, v          | 12, x          |                |                |                |                |
| 5    | yxtvuz  |                | 12, x          |                |                |                |                |
| 6    | yxtvuz  |                |                |                |                |                |                |

## P5 Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z

![P5 Diagram](Week09P5.png)

|     | z   | x   | v   |
| :-: | :-: | :-: | :-: |
| z   | 0   | 2   | 6   |
| x   | ∞   | ∞   | ∞   |
| v   | ∞   | ∞   | ∞   |

## P11 Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm

![P11 Diagram](Week09P11.png)

### P11a When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?

- c(w, x) = 5
- c(y, x) = 4
- c(z, x) = 50

### P11b Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer

There will not be a count to infinity problems as long as all routers can reach each other, and one does not attempt to communicate to a router that has gone offline.

### P11c How do you modify c(y,z) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?

c(z, y) could be increased such that going through y is no longer the shortest path to x to prevent traffic being sent through y->z before the new c(y, x) is advertised to z.
