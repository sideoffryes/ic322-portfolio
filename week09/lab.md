# Loopbacker: Week 07 Wireshark Lab

## IP Wireshark Lab

## Introduction

This lab is about learning how the Internet Protocol (IP) and the Network Layer work and exchange messages across a network.

## Collaboration

I did not collaborate with any other students.

## Process

I followed the instructions provided from the textbook website and used the sample traces provided instead of capturing my own traffic.

## Lab Questions

1. My IP address is: **192.168.86.61**

2. The TTL value is: **1**

3. The value in the upper layer protocol field is **UDP**

4. The IP header is **20 bytes**

5. The payload is **36 bytes**. I calculated this by taking the total length of the packet, 56, and subtracting the header length, 20. $56 - 20 = 36$.

6. **No**. I looked at several UDP packets, and all of their fragment offsets were set to 0.

7. The **destination port number** is the only field that changed every single time.

8. Almost **all other fields** except TTL remain constant because it is the same information.

9. The value of the identification field **increment by 1 with each packet**.

10. The protocol is **ICMP**.

11. **No**. They are in a seemingly **random order**.

12. **No**. Many of the packets have the same or similar TTL values, but there are **many different values**.

13. **Yes**. You can tell the segment has been fragmented, because the more fragments flag has been set in the IP header.

14. The **more fragments** flag.

15. The **fragment offset** flag is set to 0.

16. This IP datagram is **1500 bytes**.

17. The **fragment offset** flag is set to **1480**.

18. The **fragment offset and header checksum** change.

19. The **more fragments** flag is *not* set.

20. The source IP address is **2601:193:8302:4620:215c:f5ae:8b40:a27a**.

21. The destination IP address is **2001:558:feed::1**.

22. The value of the flow label is **0x63ed0**.

23. The payload length is **37 bytes**.

24. The upper layer protocol is **UDP**.

25. **1 IP address* is returned.

26. The IP address returned is **2607:f8b0:4006:815::200e**.
