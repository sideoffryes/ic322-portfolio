# Learning Goals Answers

1. I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm

    In a **Link State routing algorithm**, everything about the network, including all of the routers and link costs, are known to all nodes of the network. Each node broadcasts packets to all other nodes in the network with its connected links and their costs. This way, every node knows everything about every link in the network.

    The least-cost path is found by comparing the length of the available paths at each iteration. If we are starting at node x, an LS algorithm will choose its next step based on the least-costly available path. This continues until there are no more paths remaining. At this point, the least cost path from each destination node back to the source node will have been determined.

    A common *disadvantage* associated with **LS algorithms** is "oscillation." This describes a situation where routers alternate sending traffic in a clockwise and counter clockwise pattern as a result of find a new zero-cost path. This problem can be addressed by ensuring that all of the routers are *not* executing the algorithm at the same time. Research has indicated that there is a possibility of routers self-synchronizing, but this can also be addressed by randomizing the amount of time in between link advertisements from the router.

    The *advantage* of **LS algorithms** is robustness. The fact that each router calculates its own routing tables somewhat isolates routers if there is an error or the router becomes corrupt.

    A common example of a **Link State algorithm** is *Open Shortest Path First (OSPC)*. OSPF is considered an *Interior Gateway Protocol (IGP) and is used to collect and distribute network topology information information to routers in a network.

2. I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm

    A **Distance Vector algorithm** is iterative, asynchronous, and distributed. Each node received information from its *directly attached* neighbors, performs some calculations, and sends the results to its neighbors. The process continues until no more information is being exchanged between nodes that are neighbors. The algorithm also does not require each node to perform its calculations and send out information at the same time, so it is *asynchronous*. Each node stores a vector comprised of a destination node and the cost to get there. Each node will calculate its own vector for sending data from itself to all other known destinations and send its updated vector to its neighbors. The neighbors then use that vector to update its own vector. This process continues until all nodes have the most up to date vector for the shortest path to all other nodes.

    The least costs are related by the Bellman-Ford equation:

    $d_x(y)=min_v(c(x,v)+d_v(y))$

    The $min_v$ equation is applied to all of a node's neighbords to determine their costs before choosing the least costly path.

    A significant disadvantage of **DV algorithms** is broadcasting incorrect information to other hosts. If a node broadcasts information that incorrectly advertises the node to be the fastest path to another node, other nodes may flood it with traffic under the assumption that is is the fastest. They will not step sending traffic until an updated/correct vector is distributed to all nodes.

    An advantage of **DV algorithms** is message complexity. Nodes need only to talk to their neighbors, and they only need to update their routing information if a new vector provides a better path to a destination.

    A common example of a **Distance Vector algorithm** is the *Routing Information Protocol (RIP).*
