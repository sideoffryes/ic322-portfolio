# Week 15: TLS and Network Security

## MIDN 2/C Henry Frye, IC322 Fall AY24

## Links to this week's work

[Learning Goals](learningGoals.md)

[Review Questions](reviewQuestions.md)

[Security Spelunker Deep Dive](https://docs.google.com/document/d/1h3Z-akR_AoZqeRJ0TdFEdNhHglSBfobYT9NFSU2xerY/edit?usp=sharing)

[Week 13 Partner Feedback](https://github.com/chazzconi25/IC322-Portfolio/issues/20)
