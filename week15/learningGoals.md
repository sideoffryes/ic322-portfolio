# Learning Goals Answers

1. I can explain how two strangers are able to exchange secret keys in a public medium.

    Two strangers can exchange secret keys in a public medium using a **public key system**. A famous example of a public key system in the **Diffie-Hellman Key Exchange**. In public key cryptography, each user has a *public key* and a *private key*. The *public* key is available to the entire world, and the *private* key is known only to the user.

    When a user wants to send another user an encrypted message, the user begins by fetching the other user's public key. The sender then encrypts their message using the receiver's public key and a known encryption algorithm. The receiver receives the encrypted message and uses their private key and a known decryption algorithm to decrypt the message. Using this method, users are able to exchange encrypted messages without having to exchange any secret information ahead of time.

2. I can walk through the TLS handshake and explain why each step is necessary.

    1. The client sends a list of algorithms it supports. This is necessary to establish a common algorithm that both the server and client can support.

    2. The server chooses a symmetric algorithm, a public key algorithm, and HMAC keys. This step is necessary so that both sides of the transmission understand how to encrypt and decrypt messages. If this step did not happen, either side would not be able to decrypt a message from the other since they do not know how it was encrypted in the first place. These elements are all essential for both sides to know before any encrypted communication occurs.

    3. The client verifies the certificate, extracts the server's public key, and generates the pre-master secret (PMS). The PMS is encrypted with the server's public key and sent to the server. This step is important because it makes sure that the client is talking to the intended recipient, and the PMS must be created in order to later generate the shared master secret.

    4. Using the same key derivation function, the client and server independently compute the Master Secret (MS) from the PMS. The MS is sliced to generate two encryption and two HMAC keys. If the chosen symmetric algorithm uses CBC, two Initialization Vectors (IVs) are also created from the MS. This step is important because it generates the cryptographic materials necessary to begin encrypted communications and allow each side to decrypt messages.

    5. The client sends the HMAC of all the handshake messages. This is important to prevent client messages from being tampered with.

    6. The server sends the HMAC of all the handshake messages. This is important to prevent server messages from being tampered with.

3. I can explain how TLS prevents man-in-the-middle attacks.

    **HMAC** and **sequence numbers** help prevent man-in-the-middle attacks. Each end of the exchange tracks the sequence number and increments the count for each TLS record that is sent. The number is not included in record, but when the HMAC is calculated, the sequence number is included in the calculation. The recipient of the messages can verify that they have been received in the correct order by tracking the sequence numbers and performing their own HMAC calculation. The HMAC in the TLS record can then be compared with the HMAC record calculated by the receiver. If they do not match, the record has been received out of order.
