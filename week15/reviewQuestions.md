# Review Question Answers

## Chapter 8

### R20 In the TLS record, there is a field for TLS sequence numbers. True or false?

False. The only indication of the sequence number is that it is included in the HMAC calculation.

### R21 What is the purpose of the random nonces in the TLS handshake?

A nonce is a single-user number that a protocol will only use once and never again. The randomization of nonces makes it more difficult for encryption calculations to be repeated.

### R22 Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear

True. The *initialization vector (IV)* is always sent in the clear. This is considered to be safe, because even if it is intercepted, the interceptor doesn't know the key to decode the blocks.

### R23 Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?

This will be discovered during steps 5 and 6 when the client and the server exchange HMACs. When one receives the other's HMAC, they will recreate the expected value for themselves and discover that they don't match.

### P9 In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, SA and SB, respectively. Alice then computes her public key, TA, by raising g to SA and then taking mod p. Bob similarly computes his own public key TB by raising g to SB and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising TB to SA and then taking mod p. Similarly, Bob calculates the shared key S' by raising TA to SB and then taking mod p

#### P9a Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S'

$TA = g^{SA} \mod p$

$TB = g^{SB} \mod p$

$S = TB ^ {SA} \mod p$

$S' = TA ^ {SB} \mod p$

$S = S' = TB ^ {SA} \mod p = TA ^ {SB} \mod p$

${g ^ {SB}} ^ {SA} \mod p = {g ^ {SA}} ^ {SB} \mod p$

$g ^ {SB * SA} \mod p = g ^ {SB * SA} \mod p$

#### P9b With p = 11 and g = 2, suppose Alice and Bob choose private keys SA = 5 and SB = 12, respectively. Calculate Alice’s and Bob’s public keys, TA and TB. Show all work

$p = 11, \: g = 2, \: SA = 5, \: SB = 12$

$TA = g^{SA} \mod p = 2^5 \mod 11 = 10$

$TB = g^{SB} \mod p = 2^12 \mod 11 = 4$

#### P9c Following up on part (b), now calculate S as the shared symmetric key. Show all work

$S = TB ^ {SA} \mod p = 4 ^ {5} \mod 11 = 1$

#### P9d Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy

![Answer diagram](P9d.jpg)

### P14 The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?

Using a MAC is considered to be more "lightweight" because it only requires a key and taking a hash, which promotes OSPF being faster. Digital signatures requires public key infrastructure and a certificate authority.

### P23 Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram

![Figure 8.28](Week15Ch8P23.png)

No, the datagram will not be forwarded to branch-office network. After the datagram has been decrypted, R2 will realize that the sequence number is out of order because it has already been received, and it will be dropped.
