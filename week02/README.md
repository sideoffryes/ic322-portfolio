# Week 02: The Application Layer

*MIDN 2/C Henry Frye, IC322 Fall AY24*

This week, we discussed the application layer. The application layer describes how applications on various computers can communicate with each other over a network, either on the same computer or on multiple computers that could be geographically separated.

## Links to this week's work

[Building a Simple Server Lab](serverLab.md)

[Learning Goals](learningGoals.md)

[Review Questions](questions.md)
