# TCP Python Server Lab

## Introduction

This lab was all about understanding how TCP connections work in the context of setting up a webserver that would receive requests from a browser and is able to respond with appropriate HTTP response messages and returning the required resources.

## Collaboration

I did not collaborate with anyone else on this assignment.

## Process

I followed all of the instructions posted on the class website. In order to correctly format the HTTP response messages, I read the HTTP response section of the text book and used a diagram of HTTP messages to properly format my information. I also did research online to find out more about the different header values and the different options for their values. After this, I was able to complete step 2. For step 3, I was able to parse the file name by using the split method on the request packet to isolate the file in the GET request. I then used some of the framework coded from the textbook provided in the lab instructions to open the file and read the contents. I then built off my work from step 2 to send an HTTP response message that also included the contents of the requested file in the body section. I was able to get my server to successfully send one file at a time, but I had to do some more research and debugging to send multiple files. I found that I had a line indented at the wrong level that was causing my server program to exit after a singular TCP connection instead of keeping the "welcome" socket open for more traffic. After correcting the indentation, I found that I had solved my issue will handling multiple HTTP requests from the client, and I was able to return multiple HTTP responses, completing part 3. Part 4 took me considerably longer than all previous parts. I followed the hints on the course page for how to handle images, and I researched possible values for the *Content-type* header. I also compiled a list of common file extensions for images. I started with a system where I would examine the file extension from the HTTP request URL, and if the extension was in my list of image extensions, I would open the requested path in byte mode, read the contents in, and return attempt to pass it into my HTTP response message. A snippet from that version of the program looked something like this:

```python
requestedFile = request.split()[1]
extension = requestedFile.split('.')[1]
imgExtList = ["apng", "avif", "gif", "jpeg", "png", "svg+xml", "webp", "jpg"]
if extension in imgExtList:
    file = open(requestedFile[1:], "r+b")
    contentType = "image/" + extension
```

I would then iterate through the file object and pass the contents into my HTTP response. Long story short, I ended up with a debugging headache with various errors relating to trying to concatenate strings with bytes. I decided that the simpler solution was to treat all of my information as bytes. I also implemented a second file extension check to see if the requested object belonged in the *text* category. My final product looks like this:

```python
requestedFile = request.split()[1]
extension = requestedFile.split('.')[1]
imgExtList = ["apng", "avif", "gif", "jpeg", "png", "svg+xml", "webp", "jpg"]
textExtList = ["plain", "css", "html", "javascript", "js"]
# Read file in binary mode if it's an image
if extension in imgExtList:
    file = open(requestedFile[1:], "r+b")
    contentType = "image/" + extension
# Read in requested file from the file system as normal file
elif extension in textExtList:
    file = open(requestedFile[1:])
    contentType = "text/" + extension
# Try and catch all other file types
else:
    file = open(requestedFile[1:], "r+b")
    contentType = "text/plain"

lines = file.readlines()
fileData = b""
for line in lines:
    try:
        fileData += bytes(line, encoding="utf8")
    except:
        fileData += line
```

Following this version, I was able to successfully return image objects to the browser, completing part 4. I completed this lab through a combination of the textbook and various websites about HTTP, sockets, and webservers.

## Lab Questions

1. Why are we focusing on the TCP server in this lab rather than the UDP server?

    In the case of a webserver, we want to use a *TCP* connection to ensure that the user has reliable access to the content that they are attempting to retrieve from the webserver. The *TCP* protocol ensures that every byte sent between the client and the server is received by each, and thus ensures that all of the content is delivered to the user. If we implemented the *UDP* protocol, the client and the server would just "hurl" the information at each other and hope that the other received it. This is not useful in our implementation of a webserver, because the user could end up seeing a web page with only half of the intended content. In addition, HTTP always travels over TCP.

2. Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: cr and lf. What are these characters and how do we represent them in our Python response string?

    The `cr` character is short for *carriage return*, and the `lf` character is short for line feed. The `cr` character instructs the print head to return to the *beginning* of the line, this does not increment the print head to the *next* line. We would use the `\r` escape character to represent this in Python. The `lf` character instructs the print head to move to the *next* line. The `lf` character is represented by `\n` in python.

3. When a client requests the "/index.html" file, where on your computer will your server look for that file?

    The server will look for the file in the *same directory* that the script running the server is in.
