# Review Question Answers

### R5: What information is used by a process running on one host to identify a process running on another host?

A process identifies a process on another computer by using a destination address and a port number. A destination address helps specify the target computer, and the port number helps direct traffic to specific applications programmed to list for traffic on specific ports.

### R8: List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service

1. Reliable Data Transfer, *TCP*
2. Throughput, *UDP*. *UDP* enables a developer to quickly send a large amount of data to a socket, because *UDP* lacks congestion controls and will send data as quickly as possible, regardless of if the data is actually arriving intact.
3. Timing, *TCP*. *TCP* provides controls such as *timers* to control when and how often segments are sent, providing more control of the flow of information over the connection.
4. Security, *TCP*

### R11: What does a stateless protocol mean? Is IMAP stateless? What about SMTP?

A **stateless protocol** does *not* maintain any information about the clients that use it.

IMAP is *not stateless* because it needs to track users and clients in order to make sure clients can receive emails specific to them. SMTP is *not* stateless, because it does not need to track anything about the client in order to send an email.

### R12: How can websites keep track of users? Do they always need to use cookies?

Websites can track users by assigning them ID numbers in their backend databases that will also be stored locally in the client browser in the form of a cookie. Websites can also track information provided in your HTTP request messages and track the source address of the traffic.

### R13: Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?

Web caching reduces the delay in receiving objects by providing an access point to the requested objects that is geographically closer to the user. When the user requests an object that is not already cached in the web cache, the web cache will forward that request to the origin servers, and when the object is received, the web cache will maintain a copy of it. This way, the next time that a user needs to access the same object, the web cache can return the locally stored copy rather than forwarding the request to the origin servers. These web cache servers can be geographically distributed across a wide area to allow for faster access for more clients. A web cache will only reduce delay for objects that are already in the cache, and the object must be the most recent version. If the object is not in the cache or is out of date, the request will just go to the origin servers anyways.

### R14: Telnet into a Web server and send a multiline request message. Include in the request message the If-modified-since: header line to force a response message with the 304 Not Modified status code

```HTTP
telnet gaia.cs.umass.edu 80

GET /cnrg_imap.jpg HTTP/1.1
Host: gaia.cs.umass.edu
If-modified-since: Tue, 30 Oct 2007 16:59:43 GMT

HTTP/1.1 304 Not Modified
Date: Mon, 04 Sep 2023 21:42:54 GMT
Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/7.4.33 mod_perl/2.0.11 Perl/v5.16.3
ETag: "808d-43db8be4f7dc0"
```

### R26: In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support *n* simultaneous connections, each from a different client host, how many sockets would the TCP server need?

A TCP server needs two sockets, because one socket is the "welcoming" socket that makes the initial connection with the client socket, and after the initial connection is established, the server creates a new socket dedicated to the client that made the connection for the actual transportation of data. A UDP socket does not need this because it does not guarantee a connection, the server socket will just constantly listen, which the client UDP socket will just blindly send the data to the destination address; *UDP* is considered to be a "stateless" protocol, because it does not form a complete connection with the destination.

To support *n* simultaneous connections, a TCP server would need *n + 1* sockets.
