import socket, sys
serverPort = 12050
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.bind(("localhost", serverPort))
serverSocket.listen(1)

print("The server is ready to receive on host: localhost, Port:", serverPort)

while True:
    connectionSocket, addr = serverSocket.accept()
    request = connectionSocket.recv(1024).decode()
    print(request)

    try:
        if "GET" in request:
            requestedFile = request.split()[1]
            extension = requestedFile.split('.')[1]
            imgExtList = ["apng", "avif", "gif", "jpeg", "png", "svg+xml", "webp", "jpg"]
            textExtList = ["plain", "css", "html", "javascript", "js"]
            # Read file in binary mode if it's an image
            if extension in imgExtList:
                file = open(requestedFile[1:], "r+b")
                contentType = "image/" + extension
            # Read in requested file from the file system as normal file
            elif extension in textExtList:
                file = open(requestedFile[1:])
                contentType = "text/" + extension
            # Try and catch all other file types
            else:
                file = open(requestedFile[1:], "r+b")
                contentType = "text/plain"

            lines = file.readlines()
            fileData = b""
            for line in lines:
                try:
                    fileData += bytes(line, encoding="utf8")
                except:
                    fileData += line
        else:
            fileData = b""

        # Make HTTP message
        status = "HTTP/1.1 200 OK\r\n"
        h1 = "Connection: close\r\n"
        h2 = "Date: Today\r\n"
        h3 = "Server: Henry's Laptop\r\n"
        h4 = "Last-Modified: Today\r\n"
        h5 = "Content-Length: "+ str(len(fileData)) + "\r\n"
        h6 = "Content-Type: "+ contentType +"\r\n"
        blank = "\r\n"
        response = status + h1 + h2 + h3 + h4 + h5 + h6 + blank
        print(response)
        byteResponse = bytes(response, encoding="utf8") + fileData

        connectionSocket.send(byteResponse)
        connectionSocket.close()

    except IOError:
        status = "HTTP/1.1 404 Not Found\r\n"
        h1 = "Connection: close\r\n"
        h2 = "Date: Today\r\n"
        h3 = "Server: Henry's Laptop\r\n"
        response = status + h1 + h2 + h3

        connectionSocket.send(response.encode())
        connectionSocket.close()
        print(response)
serverSocket.close()
sys.exit()