# Learning Goal Answers

1. I can explain the HTTP message format, including the common fields.

    HTTP messages can be split into two categories: Request and Response messages.

    **HTTP Request** messages begins with a *request* line and is followed by *header* lines. The *request* line contains three fields: the method field, the URL field, and the HTTP version field. Methods include: GET, POST, HEAD, PUT, and DELETE, all of which specify different actions they would like the webserver to perform and which information the response message should include. The request field includes the address to the specific resource that the message wants from the webserver. The HTTP version field specifies which HTTP version the response message should conform to.

    **HTTP Response** messages has three sections: a status line, six header lines, and the entity body. The status line contains three fields: the protocol version, a status code, and the status message. These fields relay which version of the HTTP protocol was used, and the status in response to the request message. There are various status numbers with corresponding status messages to convey the webserver's response to the request. The header lines return important information about the status of the connection, the date and time the response was sent, information about the server that sent the response, when the requested resource was last modified, how long the resource is, and the type of resource. The entity body section contains the actual resource that was requested from the server.

2. I can explain the difference between HTTP GET and POST requests and why you would use each.

    **HTTP GET** requests do not use the entity body field of the HTTP message, rather they transmit the data that is being passed to the server in the URL. This can look very messy and is not secure if the user is trying to submit sensitive data. It is not considered secure because the information the user entered is displayed in the URL, so if a malicious actor was on the same network and was able to use a tool like Wireshark to sniff packets on the network, they would be able to read the sensitive material in the URL.

    **HTTP POST** requests use the entity body field to pass information to the server, and the information will not appear in the URL. This method is typically used when users fill out and submit forms.

3. I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.

    **CONDITIONAL GET** requests are GET requests typically used in conjunction with web caches. The GET request will contain an extra line that specifies that the HTTP GET message should only be forwarded to the origin server if the requested resource has been modified since the date specified in the CONDITIONAL GET request. These requests solve the issue of receiving out-of-date content from web cache servers. A CONDITIONAL GET request might look like:

    ```HTTP
    GET /week02/learningGoals.md HTTP/2 <br> 
    Host: www.gitlab.com <br>
    If-modified-since: Tue, 29 Aug 2023 13:03:48 
    ```

4. I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.

    An **HTTP response code** indicates the status of the request message. They are used in the response message for the server to tell the client if the server was able to fulfill the request. Some common codes include:
    - 200 OK: The request was successful, and the requested information is included in the response
    - 301 Moved Permanently: The object the request message asked for has been moved to a new location, and the URL of the new location is included in the response message
    - 400 Bad Request: A generic error code that the request was not understood/could not be completed by the server
    - 404 Not Found: The requested resource doesn't exist at the specified location
    - 505 HTTP Version Not Supported: the server does not support the HTTP version specified in the request message

5. I can explain what cookies are used for and how they are implemented.

    **Cookies** are information that is stored locally in the user's browser that helps the webserver identify the user when they return to the website. The webserver will issue an ID number to the browser to keep track of the user, and the browser will add that ID number to a locally stored file that includes the server name and the ID. The website accomplishes this by calling a "setcookie()" function. Every time the website is visited, the browser will send it the information stored in the cookie file.

6. I can explain the significance HTTP pipelining.

    **HTTP pipelining** is a technology introduced as a result of HTTP/1.1 implementing persistent connections, meaning that a TCP connection would continue to be left open after a server had send the HTTP response message. With pipelining, requests for resources from the server can be made back-to-back without having to wait for replies from the server for each previous request.

7. I can explain the significance of CDNs, and how they work.

    **CDNs (Content Distribution Networks)** are a system of web caches distributed over an area geographically. The purpose of a CDN is to reduce the amount of time a user needs to wait to access resources when trying to view content on websites. Common examples of CDNs are Netflix and YouTube. A user's browser needs only to connect to the nearest CDN to obtain the content they are looking for rather than connecting to the origin servers. This also helps to evenly distribute traffic across the CDNs rather than having all users attempting to access resources on the same servers. This is accomplished by using the *CONDITIONAL GET* requests mentioned earlier. Many CDNs are co-located with ISP infrastructure, so as an *HTTP request* is traveling through the network, it will reach one of the CDN cache servers. The request will be analyzed, and the cache will check to see if it has the correct version of the resource being requested by the client. If it does, it can immediately return that resource instead of forwarding the request back to the root servers, reducing the perceived delay to the client.
