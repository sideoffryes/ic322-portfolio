# Week 05: Transport Layer Part 2: TCP and UDP
*MIDN 2/C Henry Frye, IC322 Fall AY24*

We continued our discussion on the Transport Layer, this time discussing congestion and flow control.

## Links to this week's work
[Learning Goals](learningGoals.md) <br>
[Review Questions](reviewQuestions.md) <br>
[Week04 Feedback for Josh Clark](https://gitlab.usna.edu/JoshuaClark/ic322-portfolio/-/issues/6)