# Review Questions Answers

### R17: Consider two hosts, A and B, transmitting a large file to a server C, over a bottleneck link with rate R. To transfer the file, the hosts use TCP with the same parameters (including MSS and RTT) and start their transmissions at the same time. Host A uses a single TCP connection for the entire file, while Host B uses 9 simultaneous TCP connections, each for a portion (i.e., a chunk) of the file. What is the overall transmission rate achieved by each host at the beginning of the file transfer? Is this situation fair?

Host A's transmission rate is ${L \over R}$, and Host B's transmission rate is ${L \over 9R}$. I do **not** think that this is fair. If one host is allowed to open 9 simultaneous connections while the other host only has one, the bandwidth from the server will be split 10 ways, but ${9 \over 10}$ of the bandwidth will be going to the same host.

### R18: True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of ssthresh is set to one half of its previous value

False. The value of *ssthresh* is set to ${cwnd \over 2}$, half of the side of the *congestion window*.

### P27: Host A and B are communicating over a TCP connection following RFC 5681. Host B has already received from A all bytes up through byte 96. Suppose Host A then sends two segments to Host B back-to-back. The first and the second segments contain 40 and 80 bytes of data, respectively. In the first segment, the sequence number is 97, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A

- a. In the second segment sent from Host A to B, what are the sequence number,
source port number, and destination port number?
  - Sequence Number: 137 ($97 + 40 = 137$)
  - Source Port Number: 302 (same as previous segment)
  - Destination port: 80 (same as previous segment)

- b. If the first segment arrives before the second segment, in the acknowledgment
of the first arriving segment, what is the acknowledgment number,
the source port number, and the destination port number?
  - ACK #: ACK137 (Acknowledging that segment with sequence number 137 was received)
  - Source port number: 80 (returning from the web server to the client)
  - Destination port number: 302 (returning to the client's application)

- c. If the second segment arrives before the first segment, in the acknowledgment
of the first arriving segment, what is the acknowledgment number?
  - ACK #: ACK97 (Segments arrived out of order, and segment number 97 was the last received segment)

- d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment arrives after the first timeout interval. What is the sequence
    number of the next segment that A will transmit?
  - A will resend segment 97 because it thinks that it has not been received yet.

### P33: In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the SampleRTT for retransmitted segments?

Once we receive an ACK for the segment, we cannot tell if that ACK is from the original transmission or the retransmission, so we cannot tell if our SampleRTT is accurate or not.

### P36: In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?

If segments arrive out of order, there may be duplicate ACKs for a segment that was actually properly received and we might prematurely perform a fast retransmit. It is much less likely that a packet will successfully arrive if we have already received 3 ACKs for it.

### P40: Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer

- a. Identify the intervals of time when TCP slow start is operating.
  - Slow start is operating between `rounds 0 and 6`.

- b. Identify the intervals of time when TCP congestion avoidance is operating.
  - Congestion avoidance is operating between rounds `6 and 22`.

- c. After the 16th transmission round, is segment loss detected by a triple
duplicate ACK or by a timeout?
  - Triple duplicate ACK (cwnd decreases by a factor of 2).

- d. After the 22nd transmission round, is segment loss detected by a triple
duplicate ACK or by a timeout?
  - Timeout (cwnd resets)

- e. What is the initial value of ssthresh at the first transmission round?
  - $ssthresh = {ssthresh \over 2} = {1 \over 2}$

- f. What is the value of ssthresh at the 22nd transmission round?
  - $ssthresh = {ssthresh \over 2} = {29 \over 2}$

- g. During what transmission round is the 70th segment sent?
  - Round 8 (includes segments 63 - 99).

- h. Assuming a packet loss is detected after the 26th round by the receipt of
a triple duplicate ACK, what will be the values of the congestion window
size and of ssthresh?
  - $cwnd = {cwnd \over 2} = {8 \over 2} = 4$
  - $ssthresh = {cwnd \over 2} = {4 \over 2} = 2$

- i. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple
duplicate ACKs are received at the 10th round. What are the ssthresh
and the congestion window size at the 11th round?
  - TCP Tahoe unconditionally cuts the *cwnd* to 1, so both *cwnd* and *ssthresh* are 1.

- j. Again, suppose TCP Tahoe is used, and there is a timeout event at the 22nd
round. How many packets have been sent out from the 17th round till the
22nd round, inclusive?
  - ${24 + 25 + 26 + 27 + 28 = 130 \: packets}$
