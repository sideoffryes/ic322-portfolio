# Week 04: Transport Layer: TCP and UDP

*MIDN 2/C Henry Frye, IC322 Fall AY24*

This week, we discussed TCP and UDP. Both are protocols that dictate how data is passed from one process to another, either on the same host or on different hosts. TCP provides a constant connection whereas UDP is a stateless protocol. Both have their pros and cons and different applications.

## Links to this week's work

[Wireshark TCP Lab](wiresharkTCPLab.md)

[Learning Goals](learningGoals.md)

[Review Questions](questions.md)
