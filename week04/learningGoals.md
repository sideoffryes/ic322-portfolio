# Learning Goals Answers

1. I can explain how TCP and UDP multiplex messages between processes using sockets.

    Processes can use one or more *sockets* to provide a unique entry way to collect their intended data from the *network layer*. Sockets have identifying *IP addresses* and *port numbers* that help direct data to its intended destination. **Multiplexing** refers to the process that occurs at the data's source where different data chunks are gathered together, possibly from multiple source sockets, are encapsulated with header information, and are passed to the network layer. After the data chunks arrive at their intended destination, the process of *demultiplexing* occurs where the data chunks are directed to the correct socket. 

    *TCP multiplexing* specifically requires both the *source IP* and *source port* to be identified in the header as well as the *destination IP* and *destination port*. This is because TCP creates a persistent connection, between the hosts, so the destination host must known where the data came from so it can send data back. This is also because when multiple hosts are connected to a server, each host gets their own socket at the server. Therefore, when each host is sending segments to the server, not only does the server need to know the port number, but it also needs to be able to distinguish which host the segment is from so that it can direct the segment to the matching socket.

    *UDP multiplexing* only requires the *destination IP* and *destination port* because it is connectionless, and the destination process does not need to return any traffic to the source. This tuple (*destination IP*, *destination port*) can fully identity a valid *UDP* socket, but a UDP segment will also include the *source port* of the segment's sender. This is included so that the receiving host can identity the sender if it needs to. If more than one UDP segment is received at an IP address, and they have the *same* destination port, they will both be directed to the *same* socket.

2. I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.

    - **TCP** - the main feature of TCP is *reliable data transfer*. **TCP** uses a variety of tools to ensure that all data is delivered from its source to its destination in order and intact. It also provides *congestion control*, which prevents TCP connections from flooding hosts with excessive amounts of data. **TCP** is better suited for scenarios where you want to guarantee that all data will arrive at its intended destination. Some examples are email, secure communications, instant messaging, and web pages. **TCP** ensures this through a process known as the *three-way handshake*. In this process, sockets on the client and the host exchange data to ensure that each end of the connection is ready to receive data.

    - **UDP** - a much more lightweight protocol than *TCP*, **UDP** provides the very simple service of attaching some destination information to data and sending it to the network. **UDP** adds almost nothing on top of the *Internet Protocol (IP)*, so when a developer implements **UDP**, the application is almost communicating directly to the *IP* protocol. The benefits of **UDP** are that is provides finer control of how and when data is sent, there is no delay to establish a connection, there is no resource consumption associated with connections, and the packets are smaller. Some common services that use **UDP** are DNS, video streaming, and music streaming. These all benefit from **UDP** since they need to move a large volume of data quickly and do not need to ensure that the destination to send any traffic back to the source. Both a pro and a con, **UDP** has no congestion control. This can help deliver segments faster, but it can also lead to swamping networks and overflowing router buffers. To provide for error checking, **UDP** implements checksums so that the receiver can determine if the segment has been modified/corrupted since it was sent.

3. I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.

    - **Sequence Numbers**
      - TCP views data as one continuous stream of ordered bytes. To keep track of them, TCP assigns *sequence numbers* based on the order of the byte relative to the stream. A segment's *sequence number* reflects the position of the first byte of the segment in the overall stream.

    - **Duplicate ACKs**
      - When a sender sends data to a destination, the receiving process sends messages back to the sender to indicate if it successfully received the data. In general, the receiver will send either a positive acknowledgement, an ACK, or a negative acknowledgement, a NAK. This system runs in to problems, however, if packets become corrupted. It then becomes difficult to tell which data is missing and for the sender and receiver to get on the same page. To solve this issue, we use *duplicate ACKs*. If a receiver receives a corrupt message from the receiver, it will resend an ACK for the last packet it received. If the sender receives two ACKs for the same packet, it knows that the receiver is not correctly receiving the packets.

    - **Timers**
      - A sender will attempt to solve transmission problems simply be resending the current packet. At what point does a sender stop resending? To solve this, *timers* are implemented to interrupt a sender after a certain amount of time has passed. After the timer has expired, the sender will stop the timer and take appropriate actions.

    - **Pipelining**
      - If we had to wait for every packet we sent to be acknowledged before we could send another one, it would take forever to send any data. To solve this, senders can send many packets in a row without having to wait for acknowledgments. As a result from implementing pipelining, protocols must allow for a greater range of sequence numbers because each packet must still have unique sequence numbers. Buffers must also be larger to accommodate multiple packets being buffered by both the sender and the receiver.

    - **Go-Back-N**
      - In this protocol, senders can use pipelining, but they cannot have more than *N* unacknowledged packets in the pipeline. This helps control the range of sequence numbers that need to be available and helps prevent resource exhausting from allocating buffers and avoiding packet loss. When out-of-order packets are received, they are dropped by the receiver until the correct packet is received. The out-of-order packet is discarded because it is assumed that after the sender resends the correct packet, it will resend the previously out-of-order packet anyways. A draw back of GBN is that if the pipeline is full of packets being sent to a host, but an early packet fails to make it to the host, all of the later packets must be resent. This could potentially lead to large amounts of packets being resent, somewhat unnecessarily.

    - **Selective Repeat**
      - This protocol avoids some of the issues associated with the *GBN* protocol by avoiding the unnecessary retransfer of packets and only resending packets that the sender thinks were lost or corrupt at the receiver. This protocol requires that receivers individually acknowledge packets. Packets that are out-of-order will still be acknowledged, but they will be buffered until the correct packets arrive. Packets that are received out of order will be buffered until the correct packet arrives.

    - **Sliding Window**
      - *Sliding Window* is another name for the *GBN* protocol. In this metaphor, *N* refers to the "window size" of the "sliding window." The window is the range of sequence numbers that can be used to number the packets that are being sent, and it provides control over how many packets can be sent without needing to wait for a response. The left-most sequence number has been ACKed by the receiver, the window will "slide over" to allow for another sequence number to be used.

    - All of these mechanisms are used in *TCP* to ensure *reliable data transfer*.

4. I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.

    - Dropped Packets - countdown timers help the packet-sender know that if it hasn't received a response in a certain amount of time then its packet was probably dropped and it should resend the packet. The receiver can also send multiple ACKs if an out of order packet was received to indicate to the sender that an earlier packet hasn't been received yet.

    - Out-of-Order Packets - the GBN and selective repeat protocols specify two different ways to handle packets that are out of order, and they use the sliding window concept to control how many packets can be sent at a time before waiting for an ACK.

    - Corrupted/Dropped Acknowledgements - Duplicate ACKs and timers help bother the sender and the receiver determine if the packet was properly received and if they need to retransmit or now.

    - Duplicate Packets/Acknowledgements - Sequence numbers are used to identify packets and are thus used in ACKs to indicate which packets have been received, so they are used by the hosts to identify if a packet or an ACK is a duplicate or not.
