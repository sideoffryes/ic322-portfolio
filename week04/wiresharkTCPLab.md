# Wireshark TCP Lab

## Introduction

This lab uses Wireshark to examine the process of a client establishing a TCP connection with a webserver to transmit a large file. It emphasizes topics such as *sequence numbers* and *congestion control*.

## Collaboration

I did not collaborate with anyone on this lab.

## Process

I followed all of the instructions found on the textbook website to complete the lab. I used the provided .pcap files from the textbook website instead of capturing my own traffic on Wireshark.

## Lab Questions

1. The client computer's `IP address is: 192.168.86.68`, and the `port number is: 55639.`

2. The IP address of `gaia.cs.umass.edu` is `128.119.245.12`. It is sending and receiving the TCP traffic on `port 80`.

3. The *sequence number* of the TCP SYN segment is:

    ```
    Sequence Number (raw): 4236649187
    ```

    This particular TCP segment contains the SYN flag (0x002) that identifies it as a SYN segment.
    ```
    Flags: 0x002 (SYN)
    ```

    The TCP receiver will be able to use Selective Acknowledgments because the TCP option for it is set:

    ```
    TCP Option - SACK permitted
    ```

4. The *sequence number* of the SYNACK segment is:

    ```
    Sequence Number (raw): 1068969752
    ```

    The segment has both the SYN and the ACK flags set.

    ```
    Flags: 0x012 (SYN, ACK)
    ```

    The value of the Acknowledgement field is 1:

    ```
    .... ...1 .... = Acknowledgment: Set
    ```

    The host determined the value for Acknowledgment because it is sending the segment as an ACK message to the client's SYN message.
        
5. The sequence number of the HTTP POST command is:

    ```
    Sequence Number (raw): 4236801228
    ```

    There are `152359` bytes of data in the data:

    ```
    Content-Length: 152359\r\n
    ```

    Not all of the data fit into a single segment; 106 segments were used to transmit the data:

    ```
    [106 Reassembled TCP Segments (153425 bytes): #4(1448), #5(1448), #6(1448), #9(1448), #10(1448), #11(1448), #12(1448), #14(1448), #15(1448), #20(1448), #21(1448), #22(1448), #23(1448), #24(1448), #25(1448), #26(1448), #27(1448), #30(1448), ]
    ```

6.
    - The first segment was sent at time `21:43:26.840557`.
    - The ACK for segement was sent at `21:43:26.884371`.
    - The RTT for the first segement is `0.88550 - 0.840557 = 0.044943 seconds` 
    - The RTT for the second segment is `0.745546 - 0.716923 = 0.028623`.
    - The Estimate RTT is `0.107 seconds`.

7. All of the segments are `1480 bytes` (1448 bytes of payload + 32 bytes for the header).  

8. The advertised buffer space is `[Calculated window size: 131712]`. It does not throttle any of the packets because the buffer size is the same for all of the packets, so none of them needed to be buffered.

9. There are not any retransmitted segments. I looked at the sequence numbers and the ACKs to see if there were any duplicate ACKS or sequence numbers.

10. The receiver typically acknowledges `1514 bytes` of data from the sender.

11.
    $$
    Total \: Number \: of \: Bytes = 78 + 74 + 66 + 1514 + 1514 \: bytes \\
    Total \: Time = 0.024048 \: sec \\
    Throughput = {3246 \over 0.024048} = 134980.04 \: bytes/sec
    $$
    I calculated this value by dividing the total length of the first 5 packets by the amount of time it took to send the first 5 packets.

12. The graph looks like the connection is in the "slow start" phase.

13. The "fleets" seem to occur approximately every 0.025 seconds.
