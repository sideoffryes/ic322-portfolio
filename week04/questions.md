# Review Questions Answers

### R5 Why is it that voice and video traffic is often sent over TCP rather than UDP in today’s Internet? (Hint: The answer we are looking for has nothing to do with TCP’s congestion-control mechanism.)

When we are communicating over voice or video, we want to ensure that we are able to understand the people that we are communicating with, so it is important that all of the data from the sender is received by the receiver(s). Because of this, we use TCP due to its emphasis on reliable data transfer. If we were using UDP, it might become difficult to understand the conversation or view the video if packets were lost or corrupted. It has become more feasible over time as technology as improved and new protocols are implemented that increase the speed of TCP connections.

### R7 Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?

The packets from both Host A and Host B with both be directed to the same socket at Host C. Host C can differentiate the segments because UDP segments also include a field for the source port, so the two UDP segments will most likely have different source port fields. It is possible, however, that they would be the same.

### R8 Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain

Host C will have a "welcome socket" bound to port 80 to receive HTTP traffic. When Hosts A and B make connections with host C, they will initially contact the socket on port 80, but after the handshake is completed, host C will create a unique socket for each host that will handle their traffic. Each socket will be bound to a random available port, and the "welcome socket" will send that port number back to the respective hosts.

### R9 In our rdt protocols, why did we need to introduce sequence numbers?

We introduced *sequence numbers* to ensure that data received in a *TCP connection* arrives in order to accurately replicate the data stream from the sender. *Sequence numbers* are used by both the sender and receiver to identify which segments have already been sent/received and which ones the receiver is still expecting. This is important when determining if packets have been received out-of-order, and it is useful for ACKs to indicate which segments have already been received. This is implemented when determining if a segment is a retransmission.

### R10 In our rdt protocols, why did we need to introduce timers?

We introduced timers because a common solution to corrupted or lost packets is simply to resend the packet. The problem with this strategy is that the sender will continue to repeatedly send packets even if it is not possible to deliver the packet intact and will not close the connection. To solve this issue, we implemented timers to cut off the sender after a certain amount of time to take appropriate corrective actions for the scenario.

### R11 Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain

Yes. Even if the roundtrip delay is know, it is very difficult to anticipate any other delays associated with sending the packet. In addition, it is difficult to determine when exactly a packet has been lost. Because of this, the best solution is to pick a time and use a timer to control when packets will be resent.

### R15 Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110

- a) How much data is in the first segment?
  - `20 Bytes`. The sequence numbers mark the first byte in each packet, so the answer can simply be calculated by $110 - 90 = 20$.

- b) Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number?
  - Host B will send acknowledgment number `90`. An acknowledgment number is used to indicate the next byte that the host is expecting, and Host B is still waiting for the first segment to arrive, so it will send the sequence number for the first segment.

### P3 UDP and TCP use 1s complement for their checksums. Suppose you have the following three 16 bit words: 0101001101100110; 0111010010110100; 0000110111000001. What is the 1s complement of the sum of these words? Show all work. Why is it that UDP offers a checksum? With the 1’s complement scheme, how does the receiver detect errors? Describe how a single bit flip can be detected

- The sum of the words is:

$$
0101001101100110 + 0111010010110100 + 0000110111000001 = 01101010111011011
$$

- The 1s complement of the words is:

$$
10010101000100100
$$

- UDP offers checksums in order to verify that the information arrived at the destination intact. The receiver verifies the data by adding all four of the checksums together, which should be a word of all 1s. If it is not, then the receiver knows that the data may have errors.
