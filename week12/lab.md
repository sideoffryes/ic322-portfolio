# Week 12 Lab

## Protocol Pioneer Act II

## Collaboration

I was partners with MIDN 2/C Caleb Koutrakos for this lab.

## Process

We approached this lab by combining our ideas with concepts we have learned throughout the semester to attempt to improve the given client and server strategies.

## Lab Questions

*I cannot start the lab anymore for some reason, so I am recounting everything from memory.*

Caleb and I first attempted to improve the current algorithm by "cheating" and using global variables in the code to give the clients and servers universal knowledge of what was going on in the network. The intention was to control who was transmitting such that only one transmission would be sent at a time. After reading several articles about how to use global variables in python, we implemented a solution. Unfortunately, it was worse than the default algorithm. Although our method almost guaranteed message delivery, it was quite slow and led to large build-ups of queued messages. It also did not work when two clients/servers transmitted at the same time. Since both hosts thought it was OK to transmit, they would transmit at the same time and not update the global variable until after the transmission had already been sent.

We later scrapped the global variable idea and attempted to implement timers into our algorithm. The intention was to establish a window of time for each client to send messages during. Host 1 would get a 0-30 tick window, and Host 2 would get a 31-60 tick window.

We only achieved a rate of about 0.08.

Our strategy was OK but not very efficient.

I would have liked to implement a strategy that was able to resend messages if they were not successfully received.
