# Review Question Answers

## R4 Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop < L/R? Why or why not?

**Yes**. The propagation delay is defined as $L \over R$, so there will be a collision when the first packet does not reach the other node by the time the second packet is sent from that node.

## R5 In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?

The desirable characteristics:

1. When only one node has data to send, that node has a throughput of *R* bps.
2. When *M* nodes have data to send, each of these nodes has a throughput of $R \over M$ bps. This need not necessarily imply that each of the *M* nodes always has an instantaneous rate of $R \over M$, but rather that each node should have an average transmission rate of $R \over M$ over some suitably defined interval of time.
3. The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
4. The protocol is simple, so that it is inexpensive to implement.

Slotted ALOHA characteristics:

- Simple and inexpensive
- Decentralized
- Slots are of size $L \over R$ and allows for transmission at the full speed of the channel.

Token Passing Characteristics:

- Decentralized and highly efficient
- Multiple active nodes have throughput of $R \over M$ bps
- Individual active node has throughput of *R* bps

## R6 In CSMA/CD, after the fifth collision, what is the probability that a node chooses K = 4? The result K = 4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?

$P(K = 4) = {1 \over {2^5-1}} = {1 \over 31}$

Time = ${{K * 512} \over {10^6}} = 0.002048$ seconds

## P1 Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used

|          |   |   |   |   | Parity bit |
|:--------:|:-:|:-:|:-:|:-:| :-: |
|          | 1 | 1 | 1 | 0 |  1  |
|          | 0 | 1 | 1 | 0 |  0  |
|          | 1 | 0 | 0 | 1 |  0  |
|          | 0 | 1 | 0 | 1 |  0  |
|Parity bit| 0 | 1 | 0 | 0 |  1  |

## P3 Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data

Internet (in ascii): 73 110 116 101 114 110 101 116 46

$$
\begin{align*}
01001001 \\
01101110 \\
01110100 \\
01100101 \\
01110010 \\
01101110 \\
01100101 \\
01110100 \\
00101110 \\
=01110111 \\
=10001000
\end{align*}
$$

## P6 Consider the previous problem, but suppose that D has the value

### P6a 1000100101

$$
G = 10011 \\
D = 10000100101 \\
R = {D \mod G} = {10000100101 \mod 10011} = {1061 \mod 19} = 16
$$

### P6b 0101101010

$$
G = 10011 \\
D = 0101101010 \\
R = {D \mod G} = {0101101010 \mod 10011} = {362 \mod 19} = 1
$$

### P6c 0110100011

$$

G = 10011 \\
D = 0110100011 \\
R = {D \mod G} = {0110100011 \mod 10011} = {419 \mod 19} = 1

$$

## P11 Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on

### P11a What is the probability that node A succeeds for the first time in slot 4?

$$p(1-p)^{N-1} = p(1-p)^3$$

### P11b What is the probability that some node (either A, B, C or D) succeeds in slot 5?

$$Np(1-p)^{N-1} = 4p(1-p)^3$$

### P11c What is the probability that the first success occurs in slot 4?

$$Np(1-p)^{N-1} = 4p(1-p)^3$$

### P11d What is the efficiency of this four-node system?

$$Np(1-p)^{N-1} = 4p(1-p)^3$$

## P13 Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?

$$

{{Q} \over {{R \over Q} + d_{poll}}}

$$
