# Week 12: Link Layer

## MIDN 2/C Henry Frye. IC322 Fall AY24

## Links to this week's work

[Learning Goals](learningGoals.md)

[Review Questions](reviewQuestions.md)

[Protocol Pioneer Act II](lab.md)

[Week 10 Partner Feedback](https://gitlab.usna.edu/ForrestYork/midn-york-repository/-/issues/21)
