# Learning Goals Answers

1. I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem

    - ALOHA
        - *Pure* ALOHA: When a frame first arrives at the Link Layer, it is immediately transmitted into the broadcast channel. If there is a collision, there is a probability *p* chance that the frame will be immediately retransmitted. If not, the node will wait for the same amount of time it would take to transmit a frame. After the waiting period is over, there is once again a probability *p* chance that the frame will be retransmitted, and there is a 1-*p* chance that the node will wait again.

        - *Slotted* ALOHA: Slotted ALOHA is very similar to pure ALOHA except the windows of time that frames are transmitted within are broken up into identical slots. Frames are only transmitted at the beginning of slots, and the slots are synchronized between all of the nodes. If a collision occurs during a slot, the collision will be reported back to the node before the conclusion of the slot.

        - The ALOHA protocol is advantageous because it allows nodes to transmit at their full rate, and it is decentralized. This works particularly well when there is only one active node. The efficiency of ALOHA decreases as there are more active nodes on the network. The efficiency of ALOHA is $Np(1-p)^{N-1}$, where $N$ is the number of active nodes and $p$ is the probability a frame is transmitted during a slot.
    - Carrier Sense Multiple Access (CMSA) with Collision Detection (CD)
        - CMSA/CD protocols are founded on two fundamental rules:
            1. Listen before speaking
            2. If someone else begins talking at the same time, stop talking
        - One of the fundamental differences between ALOHA/other random access protocols and CMSA/CD protocol is that CMSA/CD protocols consider the activity of other nodes in the network. In CMSA/CD, once a network adapter senses that the channel is idle, it will transmit a buffered frame. If the channel is already busy, it will wait for it to become free. If the adapter does *not* detect energy from another adapter during the transmission, it assumed that the frame was transmitted successfully. On the other hand, the second that a transmission from another adapter is detected, the transmission will be aborted immediately. If the adapter needs to abort, it will wait a random amount of time before retrying.

2. I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC

    - Parity Bits - In an *even* parity scheme, the sender will count the total number of 1s the information that is to be sent. The sender will then add either a 0 or a 1 to the end of the data such that the total number of 1s is an even number. For example, if there are 5 1s in the data, the sender will add an extra one to the end such that there are now 6 1s. If there were already an even number of 1s in the data, the sender would simply attach a zero to the end. The issue with a single parity bit, however, is that if an even number of bits flip during transmission, the error will be undetectable upon reception.

    - 2D Parity - This error checking method will expand on parity bits by taking the same steps as parity bits except it will do it for the rows and columns of data. This way, it is more likely that if a bit is flipped, it will be detected since it is less likely that an even number of bits will have flipped in both the row and the column. This also makes it easier to correct the error, because the error is likely at the intersection of the row and the column with incorrect parity bits.

    - Internet Checksum - this method takes data and breaks it up into chunks of even size. The chunks are treated as integers and added together. The checksum is the one's complement of the sum, and it is included in the header of the frame. The receiver will check for errors by adding adding the checksum to contents of the message and take the one's complement. If the whole message is 0s, then there is no error.

    - Cyclic Redundancy Check (CRC) - Also known as *polynomial codes* due to the ability to view the bit string as a polynomial, *CRC codes* are an error-detection technique widely used today. The sender and receiver must first agree on a bit pattern known as the *generator*. There are international standards for 8, 12, 16, and 32-bit generators. CRC codes can also detect burst errors which are the length of the added-on bits or shorter. The sender will then choose a number of bits to append to the original message such that the total message (the original plus the new bits) is exactly divisible by the *generator*. The sender chooses the extra bits to add on such that:

    $(original \: message + extra \: bits) \mod generator = 0 \\ D * 2^{r} \:XOR\: R = nG $

3. I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ

    The **Ethernet Protocol** is the dominant protocol for LANs. It was the first widely deployed, high-speed LAN, cheaper and less complex than competitors, competitive data-transmission rates, and cheaper hardware.

    The original Ethernet LAN used a coaxial bus topology where frames transmitted from the sender were broadcasted to all other nodes on the network. The *hub* was a link-layer device that acted on individual bits, recreating incoming bits and boosting their signal onto all outgoing interfaces. This device was prone to collisions when more than one frame arrived at the hub at the same time. Ethernet LANs were revolutionized by the arrival of *switches*. A switch is *collision-less* and basically a store-and-forward packet switch. The different between a link-layer switch and a packet switch is that the packet switch operates at the *network layer*.

    Ethernet frames contain a variety of fields:

    - Data Field
        - 46 -  1500 bytes
            - If greater than 1500 bytes, must be fragmented into multiple Ethernet frames
            - If less than 46 bytes, must be padded with extra data until at least 46 bytes
        - Contains the IP datagram
    - Destination Address
        - 6 bytes
        - The *MAC address* of the destination adapter
    - Source Address
        - 6 bytes
        - The *MAC address* of the adapter which transmitted the frame.
    - Type Field
        - Enables Ethernet to *multiplex* network layer protocols
        - Enables support for multiple network layer protocols
    - Cyclic Redundancy Check (CRC)
        - 4 bytes
        - Allows receiving adapter to detect bit errors
    - Preamble
        - 8 bytes
        - "Wake up" the receiving adapter
        - Synchronize sender and receiver's clocks to compensate for links not transmitting at exactly the same rate

    Some examples of *Ethernet versions* include:

    - 10BASE-T
    - 10BASE-2
    - 100BASE-T
    - 1000BASE-LX
    - 10GBASE-T
    - 40GBASE-T

    They all follow a pattern where the first number specifies the transmission rate (in either megabits or gigabits per second), "BASE" means *baseband ethernet*, indicating that the physical transmission medium carries only Ethernet traffic, and the last part specifies the physical transmission medium (*T* typically stands for twisted pair)
